package gbc;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Memory implements MemoryInterface, Serializable {	
	AddressedMemory gbMem;
	Registers gbReg;
	Rtc clock;
	
	ByteBuffer palette;
	
	boolean colorFlag;
	boolean halt;
	
	//hdma variables
	boolean hdma;		//Are we currently doing horizontal blanking DMA?
	int lines;
	int origin;
	int destination;
	
	transient InputInterface buttons;
	
	public Memory(boolean color, File romFile, File ramFile, ByteBuffer paletteref) {
		colorFlag = color;
		gbMem = new AddressedMemory(colorFlag, romFile, ramFile);
		gbReg = new Registers(colorFlag);
		clock = new Rtc();
		halt = false;
		palette = paletteref;
	}

	@Override
	public int getRomByte() {
		//TODO check for when PC overflows. Is it at 0x8000?
		return readMappedMemory(gbReg.pc++);
	}

	@Override
	public int readRegisterVV(int vv) {
		switch (vv) {
		case 0: return getRegPair(regPair.BC);
		case 1: return getRegPair(regPair.DE);
		case 2: return getRegPair(regPair.HL);
		case 3: return getRegPair(regPair.SP);
		default: return 0;
		}
	}

	@Override
	public int readRegisterQQ(int qq) {
		if (qq == 3) return getRegPair(regPair.AF);
		else return readRegisterVV(qq);
	}

	@Override
	public void writeRegisterVV(int vv, int value) {
		switch (vv) {
		case 0: setRegPair(regPair.BC, value);
				break;
		case 1: setRegPair(regPair.DE, value);
				break;
		case 2: setRegPair(regPair.HL, value);
				break;
		case 3: setRegPair(regPair.SP, value);
		}
	}

	@Override
	public void writeRegisterQQ(int qq, int value) {
		if (qq == 3) setRegPair(regPair.AF, value);
		else writeRegisterVV(qq, value);
	}

	@Override
	public int readRegisterU(int u) {
		switch (u) {
		case 0: return gbReg.b;
		case 1: return gbReg.c;
		case 2: return gbReg.d;
		case 3: return gbReg.e;
		case 4: return gbReg.h;
		case 5: return gbReg.l;
		case 6: return this.readMappedMemory(getRegPair(regPair.HL));
		case 7: return gbReg.a;
	}
	return 0;
	}

	@Override
	public void writeRegisterU(int u, int value) {
		switch (u) {
		case 0: setReg(reg.B, value);
				break;
		case 1: setReg(reg.C, value);
				break;
		case 2: setReg(reg.D, value);
				break;
		case 3: setReg(reg.E, value);
				break;
		case 4: setReg(reg.H, value);
				break;
		case 5: setReg(reg.L, value);
				break;
		case 6: this.writeMappedMemory(getRegPair(regPair.HL), value);
				break;
		case 7:setReg(reg.A, value);
				break;
		}
	}

	@Override
	public void writeMappedMemory(int addr, int value) {
		if (value > 0xFF) value -= 0x100;
		if (value < 0) value += 0x100;
		
		//Address space for MBC registers
		if (0x0000 <= addr && addr < 0x8000) {
			gbMem.writeMBC(addr, value);
		}
		
		//Address space for vRam
		else if (0x8000 <= addr && addr < 0xA000) {
			if (colorFlag) {
				gbMem.vRam[readMappedMemory(0xFF4F) & 0x01][addr-0x8000] = value;
			}
			
			//no bank switching for original gameboy
			else {
				gbMem.vRam[0][addr-0x8000] = value;
			}
		}
		
		//Address space for external cartridge RAM
		else if (0xA000 <= addr && addr < 0xC000) {
			int bank = gbMem.mbcRamBank();
			//TODO entirely redo timing IN EMULATOR!!!
			//check if a bank was allocated and if access is allowed
			if (bank != - 1 && gbMem.mbcRamAccess()) {
				if ((gbMem.mbc == 0x10 || gbMem.mbc == 12 || gbMem.mbc == 13) && bank > 0x07 && bank < 0x0D) {
					System.out.println("Writing bank " + Integer.toHexString(bank) + " with " + value);
					switch (bank) {
					case 8: if (value > 59) throw new UnsupportedOperationException("seconds write outside 59 seconds. Check range after masking to 6 bits: " + value);
						clock.setSeconds(value);
						break;
						
					case 9: if (value > 59) throw new UnsupportedOperationException("seconds write outside 59 minutes. Check range after masking to 6 bits: " + value);
						clock.setMinutes(value);
						break;
						
					case 0x0A: if (value > 23) throw new UnsupportedOperationException("seconds write outside 23 hours. Check range after masking to 5 bits: " + value);
						clock.setHours(value);
						break;
						
					case 0x0B: if (value > 255) throw new UnsupportedOperationException("Days write outside of 255: " + value);
						clock.setDays((clock.getDays() & 0x100) & value);
						break;
						
					case 0x0C: if ((value & 0x01) == 0x01) clock.setDays(clock.getDays() & 0xFF);
						clock.setHalt((value & 0x40) == 0x40);
						clock.setCarry((value & 0x80) == 0x80);
					}
				}
				
				else {
					gbMem.cartRam[bank][addr - 0xA000] = (byte) value;
				}
			}
		}
		
		//Address space for general purpose ram.
		else if (0xC000 <= addr && addr < 0xD000) {
			gbMem.ram[0][addr-0xC000] = value;
		}
		
		else if (0xD000 <= addr && addr < 0xE000) {
			if (colorFlag) {
				int workingBank = readMappedMemory(0xFF70) & 0b00000111;
				if (workingBank == 0) workingBank = 1;
				gbMem.ram[workingBank][addr-0xD000] = value;
			}
			
			//no bank switching for original gameboy. Stuck on bank 1 in this range.
			else
			{
				gbMem.ram[1][addr-0xD000] = value;
			}
		}
		
		//Mirrored general purpose RAM. Explicitly described for clarity
		else if (0xE000 <= addr && addr < 0xF000) {
			gbMem.ram[0][addr-0xE000] = value;
		}
		
		else if (0xF000 <= addr && addr < 0xFE00) {
			if (colorFlag) {
				int workingBank = readMappedMemory(0xFF70) & 0b00000111;
				if (workingBank == 0) workingBank = 1;
				gbMem.ram[workingBank][addr-0xF000] = value;
			}
			
			//no bank switching for original gameboy. Stuck on bank 1 in this range.
			else
			{
				gbMem.ram[1][addr-0xF000] = value;
			}
		}
		
		//Address space for Object Attribute Memory
		else if (0xFE00 <= addr && addr < 0xFEA0) {
			gbMem.OAM[addr - 0xFE00] = value;
		}
		
		//Address space for CPU registers and CPU ram
		else if (0xFF00 <= addr && addr < 0x10000) {			
			//all writes to DIV register reset value
			if (addr == 0xFF04) value = 0;
			
			//masking for STAT register
			else if (addr == 0xFF41) {
				value = (value & 0b01111000) | (readMappedMemory(0xFF41) & 0b00000011);
			}
			
			gbMem.cpuRam[addr - 0xFF00] = value;
			
			//Do we begin DMA?
			if (addr == 0xFF46) gbDMA(value);
			
			//Color specific memory mapped registers
			if (colorFlag) {
				if (addr == 0xFF55 && (value & 0x80) == 0x80) {
					System.out.println("Color DMA!");
					gbcDMA(value);
				}
				
				if (addr == 0xFF69 || addr == 0xFF6B) {
					int paletteParams = 0, paletteOffset = 0;
					//TODO Add reading in readmappedmemory
					if (addr == 0xFF69) {
						paletteParams = readMappedMemory(0xFF68);
						paletteOffset = 0;		//background palette starts at 0
					}
					
					else if (addr == 0xFF6B) {
						paletteParams = readMappedMemory(0xFF6A);
						paletteOffset = 4*8*4;		//Object palette starts after 8 palettes of 4 colours of 4 elements each: RGBA.
					}

					paletteOffset += ((paletteParams & 0b00111110) >>> 1) * 4;
					
					//Print system palettes
					//XXX remove
					//System.out.println("Palette parameters: " + Integer.toHexString(paletteParams) + " with value: " + Integer.toHexString(value) + " and offset " +Integer.toHexString(paletteOffset));
					
					//write to the lower of the packed 2 byte RGB value					
					if ((paletteParams & 0x01) == 0) {
						palette.position(paletteOffset);
						palette.put((byte) ((value & 0x1F)));
						int tempGreen = palette.get();
						palette.position(paletteOffset + 1);
						palette.put((byte) (((tempGreen & 0b11000) | ((value & 0xE0) >> 5))));
					}
					
					//write to the higher byte
					else {
						palette.position(paletteOffset + 1);
						int tempGreen = palette.get();
						palette.position(paletteOffset + 1);
						palette.put((byte) ((((value & 0x03) << 3) | (tempGreen & 0b111))));
						palette.position(paletteOffset + 2);
						palette.put((byte) (((value & 0b01111100) >> 2)));
					}
					
					if ((paletteParams & 0x80) == 0x80) {
						int nextAddr = paletteParams & 0b00111111;
						nextAddr++;
						paletteParams = (paletteParams & 0xC0) | (nextAddr & 0x3F);
						
						if (addr == 0xFF69) {
							writeMappedMemory(0xFF68, paletteParams);
						}
						
						else if (addr == 0xFF6B) {
							writeMappedMemory(0xFF6A, paletteParams);
						}
					}
				}
			}
		}
	}

	@Override
	public int readMappedMemory(int addr) {		
		//Cartridge ROM space
		int value = 0;
		if (0x0000 <= addr && addr < 0x8000) {
			value = gbMem.readRom(addr);
		}
		
		//vRam space
		else if (0x8000 <= addr && addr < 0xA000) {
			if (colorFlag) {
				value = gbMem.vRam[readMappedMemory(0xFF4F) & 0x01][addr-0x8000];
			}
			
			//No bank switching for original gameboy
			else {
				value = gbMem.vRam[0][addr-0x8000];
			}
		}
		
		//TODO Cartridge RAM space
		else if (0xA000 <= addr && addr < 0xC000) {
			int bank = gbMem.mbcRamBank();
			
			//check if a bank was allocated and if access is allowed
			if (bank != - 1) {
				//TODO add clock reading
				if ((gbMem.mbc == 0x10 || gbMem.mbc == 12 || gbMem.mbc == 13) && bank > 0x07 && bank < 0x0D) {
					switch (bank) {
					case 0x08: value = clock.getSeconds();
					break;
					case 0x09: value = clock.getMinutes();
					break;
					case 0x0A: value = clock.getHours();
					break;
					case 0x0B: value = clock.getDays() & 0xFF;
					break;
					case 0x0C: value = ((clock.getDays() & 0x100) >> 8);
						if (clock.getHalt()) value &= 0x40;
						if (clock.getCarry()) value &= 0x80;
					break;					
					}
				}
				
				else {
					value = gbMem.cartRam[bank][addr - 0xA000];
				}
			}
		}
		
		//Address space for general purpose ram
		else if (0xC000 <= addr && addr < 0xD000) {
			value = gbMem.ram[0][addr-0xC000];
		}
		
		else if (0xD000 <= addr && addr < 0xE000) {
			if (colorFlag) {
				int workingBank = readMappedMemory(0xFF70) & 0b00000111;
				if (workingBank == 0) workingBank = 1;
				value = gbMem.ram[workingBank][addr-0xD000];
			}
			
			//no bank switching for original gameboy. Stuck on bank 1 in this range.
			else
			{
				value = gbMem.ram[1][addr-0xD000];
			}	
		}
		
		//Mirror general purpose RAM included here for clarity
		else if (0xE000 <= addr && addr < 0xF000) {
			value = gbMem.ram[0][addr-0xE000];
		}
		
		else if (0xF000 <= addr && addr < 0xFE00) {
			if (colorFlag) {
				int workingBank = readMappedMemory(0xFF70) & 0b00000111;
				if (workingBank == 0) workingBank = 1;
				value = gbMem.ram[workingBank][addr-0xF000];
			}
			
			//no bank switching for original gameboy. Stuck on bank 1 in this range.
			else
			{
				value = gbMem.ram[1][addr-0xF000];
			}	
		}
		
		//Address space for Object Attribute Memory
		else if (0xFE00 <= addr && addr < 0xFEA0) {
			value = gbMem.OAM[addr - 0xFE00];
		}
		
		//Address space for CPU registers and CPU ram
		else if (0xFF00 <= addr && addr < 0x10000) {
			if (addr == 0xFF00) {
				value =  0xC0 | buttons.getKeys(gbMem.cpuRam[0]);
			}
			
			else if (addr == 0xFF0F) {
				value = 0xE0 | gbMem.cpuRam[addr - 0xFF00];
			}
			
			else {
				value = gbMem.cpuRam[addr - 0xFF00];
			}
		}
		
		return value & 0xFF;
	}

	
	@Override
	public void setRegPair(regPair register, int value) {
		if (value > 0xFFFF) value -= 0x10000;
		if (value < 0) value += 0x10000;
		
		switch(register) {
		case AF:
			gbReg.f = value & 0xF0;
			gbReg.a = (value >> 8) & 0xFF;
			break;
		case BC:
			gbReg.c = value & 0xFF;
			gbReg.b = (value >> 8) & 0xFF;
			break;
		case DE:
			gbReg.e = value & 0xFF;
			gbReg.d = (value >> 8) & 0xFF;
			break;
		case HL:
			gbReg.l = value & 0xFF;
			gbReg.h = (value >> 8) & 0xFF;
			break;
		case PC:
			gbReg.pc = value;
			break;
		case SP:
			gbReg.sp = value;
			break;
		}
	}

	
	@Override
	public int getRegPair(regPair register) {
		switch(register) {
		case AF:
			return gbReg.a << 8 | gbReg.f;
		case BC:
			return gbReg.b << 8 | gbReg.c;
		case DE:
			return gbReg.d << 8 | gbReg.e;
		case HL:
			return gbReg.h << 8 | gbReg.l;
		case PC:
			return gbReg.pc;
		case SP:
			return gbReg.sp;
		default:
			return 0;
		}
	}

	
	@Override
	public void setReg(reg register, int value) {
		if (value > 0xFF) value -= 0x100;
		if (value < 0) value += 0x100;
		
		switch (register) {
		case A:
			gbReg.a = value;
			break;
		case B:
			gbReg.b = value;
			break;
		case C:
			gbReg.c = value;
			break;
		case D:
			gbReg.d = value;
			break;
		case E:
			gbReg.e = value;
			break;
		case F:
			gbReg.f = value & 0xF0;
			break;
		case H:
			gbReg.h = value;
			break;
		case L:
			gbReg.l = value;
			break;
		}
	}
	

	@Override
	public int getReg(reg register) {
		switch (register) {
		case A: return gbReg.a;
		case B: return gbReg.b;
		case C: return gbReg.c;
		case D: return gbReg.d;
		case E: return gbReg.e;
		case F: return gbReg.f;
		case H: return gbReg.h;
		case L: return gbReg.l;
		default: return 0;
		}
	}

	
	@Override
	public boolean getFlag(flag flagBit) {
		switch (flagBit) {
		case C: return (gbReg.f & 0x10) == 0x10;
		case H: return (gbReg.f & 0x20) == 0x20;
		case N: return (gbReg.f & 0x40) == 0x40;
		case Z: return (gbReg.f & 0x80) == 0x80;
		default: return false;
		}
	}


	@Override
	public void setFlag(flag flagBit, boolean value) {
		switch (flagBit) {
		case C:
			if (value) gbReg.f = gbReg.f | 0x10;
			else gbReg.f = gbReg.f & (~0x10);
			break;
		case H:
			if (value) gbReg.f = gbReg.f | 0x20;
			else gbReg.f = gbReg.f & (~0x20);
			break;
		case N:
			if (value) gbReg.f = gbReg.f | 0x40;
			else gbReg.f = gbReg.f & (~0x40);
			break;
		case Z:
			if (value) gbReg.f = gbReg.f | 0x80;
			else gbReg.f = gbReg.f & (~0x80);
			break;
		}
	}
	
	@Override
	public void setMIE(boolean value) {
		gbReg.mie = value;
	}

	@Override
	public boolean getMIE() {
		return gbReg.mie;
	}

	@Override
	public void setIE(InterruptFlag interrupt, boolean value) {
		int ieReg = readMappedMemory(0xFFFF);
		switch (interrupt) {
		case BTN: ieReg = (value) ? ieReg | 1 << 4 : ieReg & ~(1 << 4);
			break;
		case Serial: ieReg = (value) ? ieReg | 1 << 3 : ieReg & ~(1 << 3);
			break;
		case TMR: ieReg = (value) ? ieReg | 1 << 2 : ieReg & ~(1 << 2);
			break;
		case LCDC: ieReg = (value) ? ieReg | 1 << 1 : ieReg & ~(1 << 1);
			break;
		case blank: ieReg = (value) ? ieReg | 1 : ieReg & ~1;
			break;
		}
		writeMappedMemory(0xFFFF, ieReg);
	}

	@Override
	public boolean getIE(InterruptFlag interrupt) {
		int ieReg = readMappedMemory(0xFFFF);
		switch(interrupt) {
			case BTN: return (ieReg & (1 << 4)) > 0; 
			case Serial: return (ieReg & (1 << 3)) > 0;
			case TMR: return (ieReg & (1 << 2)) > 0;
			case LCDC: return (ieReg & (1 << 1)) > 0;
			case blank: return (ieReg & 1) > 0;
			default: return false;			
		}
	}

	@Override
	public void setIR(InterruptFlag interrupt, boolean value) {
		int irReg = readMappedMemory(0xFF0F);
		switch (interrupt) {
		case BTN: irReg = (value) ? irReg | 1 << 4 : irReg & ~(1 << 4);
			break;
		case Serial: irReg = (value) ? irReg | 1 << 3 : irReg & ~(1 << 3);
			break;
		case TMR: irReg = (value) ? irReg | 1 << 2 : irReg & ~(1 << 2);
			break;
		case LCDC: irReg = (value) ? irReg | 1 << 1 : irReg & ~(1 << 1);
			break;
		case blank: irReg = (value) ? irReg | 1 : irReg & ~1;
			break;
		}
		writeMappedMemory(0xFF0F, irReg);
	}

	@Override
	public boolean getIR(InterruptFlag interrupt) {
		int irReg = readMappedMemory(0xFF0F);
		switch(interrupt) {
			case BTN: return (irReg & (1 << 4)) > 0; 
			case Serial: return (irReg & (1 << 3)) > 0;
			case TMR: return (irReg & (1 << 2)) > 0;
			case LCDC: return (irReg & (1 << 1)) > 0;
			case blank: return (irReg & 1) > 0;
			default: return false;			
		}
	}
	
	@Override
	public String getRomName() {
		return gbMem.romName;
	}

	@Override
	public void attachInput(InputInterface mappedButtons) {
		buttons = mappedButtons;
	}

	public boolean GetHalt() {
		return halt;
	}

	public void SetHalt(boolean state) {
		halt = state;
	}

	public void gbDMA(int DMAOffset) {
		DMAOffset *= 0x100;
		
		for (int i = 0; i < 0xA0; i++) {
			writeMappedMemory(0xFE00 + i, readMappedMemory(DMAOffset + i));
		}
	}
	
	public void gbcDMA(int hdma5) {
		int hdma1 = readMappedMemory(0xFF51);
		int hdma2 = readMappedMemory(0xFF52);
		int hdma3 = readMappedMemory(0xFF53);
		int hdma4 = readMappedMemory(0xFF54);
		
		origin = (hdma1 << 8) & (hdma2 & 0xF0);
		destination = 0x8000 + ((hdma3 & 0x1F) << 8) & (hdma4 & 0xF0);
		lines = (hdma5 & 0x7F) + 1;
		
		System.out.println("Hdma5: " + Integer.toHexString(hdma5) + " origin: " + Integer.toHexString(origin) + " destination: " + Integer.toHexString(destination) + " lines: " + Integer.toHexString(lines));
		
		//check if we're doing an instant DMA
		if ((hdma5 & 0x80) == 0) {
			while (lines-- > 0) {
				for (int i = 0; i < 16; i++) {
					writeMappedMemory(destination + i, readMappedMemory(origin + i));
				}
				origin += 0x10;
				destination += 0x10;
			}
			writeMappedMemory(0xFF55, 0);
			hdma = false;
		}
		
		else {
			hdma = true;
		}
	}
}