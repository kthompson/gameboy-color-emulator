package gbc;

import java.io.Serializable;

public interface InputInterface extends Serializable {
	void attachToWindow(long window);
	void setKeys();
	void updateButtons();
	
	int getKeys(int currentVal);
	
	void closeInput();
}
