package gbc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

public class AddressedMemory implements Serializable {
	int vRam[][];
	int ram[][];
	int OAM[];
	int cpuRam[];
	
	//most games have multiple banks. Seeing as we don't need to do operations on the
	//instructions we can use a byte array here. Helps for reading the file as well.
	byte rom[][];
	
	//Cartridge RAM is also banked
	byte cartRam[][];
	long rtcInit, rtcLatch;
	
	//we need to keep track of which mbc is being used and its properties
	int mbc, mbcRegs[], romBanks, ramBanks;
	String romName;
	
	boolean colorFlag, romColorFlag;
	
	public AddressedMemory(boolean color, File romFile, File ramFile) {
		//We'll be making a lot of small reads from the ROM file so we need to read it into memory to reduce latency.

		try {
			byte[] fileBuffer = new byte[0x4000];
			FileInputStream inStream = new FileInputStream(romFile);
			inStream.read( fileBuffer, 0, 0x4000);			//Read one bank of ROM to get details
			
			mbc = fileBuffer[0x147] & 0xFF;			//Read MBC byte
			System.out.println("MBC - " + Integer.toHexString(mbc));
			
			romBanks = fileBuffer[0x148];		//Find the number of rom banks
			switch (romBanks) {
			case 0: romBanks = 2;
					break;
			case 1: romBanks = 4;
					break;
			case 2: romBanks = 8;
					break;
			case 3: romBanks = 16;
					break;
			case 4: romBanks = 32;
					break;
			case 5: romBanks = 64;
					break;
			case 6: romBanks = 128;
					break;
			case 7: romBanks = 256;
					break;
			case 8: romBanks = 512;
					break;
			case 0x52: romBanks = 72;
					break;
			case 0x53: romBanks = 80;
					break;
			case 0x54: romBanks = 96;
					break;
			}
			
			ramBanks = fileBuffer[0x149];
			switch (ramBanks) {
			case 0: ramBanks = 0;
					break;
			case 1: ramBanks = 1;
					break;
			case 2: ramBanks = 1;
					break;
			case 3: ramBanks = 4;
					break;
			case 4: ramBanks = 16;
			}
					
			romName = new String(fileBuffer, 0x134, 0x0F);
			if (fileBuffer[0x143] == 0x80) romColorFlag = true;
			else romColorFlag = false;
			
			//Assign the 8kB cartridge RAM bank arrays
			cartRam = new byte[ramBanks][0x2000];
			
			//Assign the 16kB bank arrays for ROM
			rom = new byte[romBanks][0x4000];
			
			//Iterate through the banks and get data from the file buffer
			for (int i = 0; i < romBanks; i++) {
				rom[i] = fileBuffer.clone();
				inStream.read( fileBuffer, 0, 0x4000);
			}
			
			//All MBCs have four registers except MBC2, which won't be too offended with an extra two regs
			mbcRegs = new int[4];
			mbcRegs[1] = 1;
			
			inStream.close();
			
			if (ramFile.exists()) loadRam(ramFile);
			else {
				ramFile.createNewFile();
				rtcInit = System.currentTimeMillis();
				System.out.println("Creating new save file.");
				
				FileOutputStream outStream;
				try {
					outStream = new FileOutputStream(ramFile);

					for (int i = 0; i < ramBanks; i++) {
						for (int j = 0; j < 0x2000; j++) {
							outStream.write(0xFF);
						}
					}
					outStream.close();
				} catch (FileNotFoundException e) {
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} 
		catch (IOException e) {		//not sure what to write in the catch
			e.printStackTrace();
		}
				
		//Most memory is garbage on reset, but some need initialisation.
		colorFlag = color;
			
		//Initialise arrays
		vRam = new int[2][0x2000];
		ram = new int[8][0x1000];
		OAM = new int[0xA0];
		cpuRam = new int[0x100];
		
		//initial register values. Array is 0 filled so explicit 0 reset values aren't set.
		//DMG values?
		cpuRam[0x02] = 0x7E;
		cpuRam[0x03] = 0xFF;
		cpuRam[0x04] = 0xFF;
		cpuRam[0x07] = 0xF8;
		cpuRam[0x10] = 0x80;
		cpuRam[0x11] = 0xBF;
		cpuRam[0x12] = 0xF3;
		cpuRam[0x14] = 0xBF;
		cpuRam[0x16] = 0x3F;
		cpuRam[0x19] = 0xBF;
		cpuRam[0x1A] = 0x7F;
		cpuRam[0x1B] = 0xFF;
		cpuRam[0x1C] = 0x9F;
		cpuRam[0x1E] = 0xBF;
		cpuRam[0x20] = 0xFF;
		cpuRam[0x23] = 0xBF;
		cpuRam[0x24] = 0x77;
		cpuRam[0x25] = 0xF3;
		cpuRam[0x26] = 0xF1;
		
		cpuRam[0x41] = 0x80;
		cpuRam[0x47] = 0xFC;
		cpuRam[0x48] = 0xFF;
		cpuRam[0x49] = 0xFF;
		
		//gameboy color CPU manual
		cpuRam[0x40] = 0x91;		//LCDC
		
		//start in vblank for easier debugging
		cpuRam[0x44] = 0x91;
	}

	int readRom(int addr) {
		int value = 0;
		
		//All MBCs map the first bank to this space
		if (0x0000 <= addr && addr < 0x4000) value = rom[0][addr];
		
		//The second part is where the MBC presents different banks
		else if (0x4000 <= addr && addr < 0x8000) {
			value = rom[mbcRomBank()][addr-0x4000];
		}
		
		//Correct the result of a cast from a negative byte to an int
		if (value < 0) value += 256;
		return value;
	}
	
	void loadRam(File input) {
		FileInputStream inStream;
		try {
			inStream = new FileInputStream(input);
			
			for (int i = 0; i < ramBanks; i++) {
				inStream.read(cartRam[i], 0, 0x2000);
			}
			inStream.close();
		} catch (FileNotFoundException e) {
			System.out.println("Cannot find save file - " + input.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void saveRam(File output) {
		FileOutputStream outStream;
		try {
			outStream = new FileOutputStream(output);

			for (int i = 0; i < ramBanks; i++) {
				outStream.write(cartRam[i], 0, 0x2000);
			}
			outStream.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//very verbose. I don't care.
	void writeMBC(int addr, int value) {
		//mbc1 and mbc3
		if (mbc == 1 || mbc == 2 || mbc == 3 || mbc == 0x10 || mbc == 0x11 || mbc == 0x12 || mbc == 0x13) {
			if (0x0000 <= addr && addr < 0x2000) {
				mbcRegs[0] = value;
			}
			
			else if (0x2000 <= addr && addr < 0x4000) {
				mbcRegs[1] = value;
			}
			
			else if (0x4000 <= addr && addr < 0x6000) {
				mbcRegs[2] = value;
			}
			
			else if (0x6000 <= addr && addr < 0x8000) {
				if (mbcRegs[3] == 0 && value == 1) rtcLatch = System.currentTimeMillis() - rtcInit;
				mbcRegs[3] = value;
			}
		}
		
		//mbc5
		else if (mbc > 0x18 && mbc < 0x1F) {
			if (0x0000 <= addr && addr < 0x2000) {
				mbcRegs[0] = value;
			}
			
			else if (0x2000 <= addr && addr < 0x3000) {
				mbcRegs[1] = value;
			}
			
			else if (0x3000 <= addr && addr < 0x4000) {
				mbcRegs[2] = value;
			}
			
			else if (0x4000 <= addr && addr < 0x6000) {
				if (mbcRegs[3] == 0 && value == 1) rtcLatch = System.currentTimeMillis() - rtcInit;
				mbcRegs[3] = value;
			}
		}
	}
	
	int mbcRomBank() {
		//TODO Implement all MBCs
		int bank = 0;
		
		//For no MBC, just map the next bank.
		if (mbc == 0) {
			bank = 1;
		}
		
		//MBC1 and hudson huc
		else if (mbc == 1 || mbc == 2 || mbc == 3) {
			//check if the controller is in 16Mb/8KB rom/ram configuration.
			if ((mbcRegs[3] & 0x01) == 0) {
				bank = (((mbcRegs[2] & 0b00000011) << 5) | (mbcRegs[1] & 0b00011111)) % romBanks;
			}
			
			//otherwise the controller is in 4Mb/32KB mode
			else {
				bank = (mbcRegs[1] & 0b00011111) % romBanks;
			}
			
			//Least significant nibble can't be equal to 0
			if ((bank & 0x1F) == 0) bank++;
		}
		
		//mbc 3
		else if (mbc == 0x10 || mbc == 0x11 || mbc == 0x12 || mbc == 0x13) {
			bank = (mbcRegs[1] & 0b01111111) % romBanks;
			if (bank == 0x00) bank = 0x01;
		}
		
		//mbc 5
		else if (mbc > 0x18 && mbc < 0x1F) {
			bank = (((mbcRegs[2] & 0x01) << 8) | mbcRegs[1]) % romBanks;
		}
		
		else if (mbc == 0xFF) {
			bank = mbcRegs[1];
		}
		
		else {
			throw new UnsupportedOperationException("MBC unsupported: 0x" + Integer.toHexString(mbc));
		}
		
		return bank;
	}
	
	int mbcRamBank() {
		//TODO Implement all MBCs
		int bank = -1;
		
		//mbc1 and hudson huc
		if (mbc == 2 || mbc == 3) {
			//check for cartridge RAM banking
			if ((mbcRegs[3] & 0x01) == 1) {
				bank = (mbcRegs[2] & 0b00000011) % ramBanks;
			}
			
			else {
				bank = 0;
			}
		}
		
		//mbc 3
		else if (mbc == 0x10 || mbc == 0x12 || mbc == 0x13) {
			//TODO add clock bank switching
			bank = mbcRegs[2] & 0x0F;
			if (bank < 4)	bank = (mbcRegs[2] & 0b00000011) % ramBanks;
			else if (bank < 0x08 && bank > 0x0C) throw new UnsupportedOperationException("mbc3 bank out of range: " + bank);
		}
		
		//mbc 5
		else if (mbc > 0x18 && mbc < 0x1F) {
			bank = (mbcRegs[3] & 0x0F) % ramBanks;
		}
		
		//hudson huc
		else if (mbc == 0xFF) {
			bank = (mbcRegs[3] & 0x0F) % ramBanks;
		}
		
		return bank;
	}
	
	boolean mbcRamAccess() {
		//TODO Implement all MBCs
		if (mbc == 0x02 || mbc == 0x03 || mbc ==0x10 || mbc == 0x12 || mbc == 0x13) {
			if ((mbcRegs[0] & 0x0F) == 0x0A) return true;
			else return false;
		}
		
		//default to true
		return true;
	}
}

