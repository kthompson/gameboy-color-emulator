package gbc;

public interface MemoryInterface {
	//**********************************************************************************
	//								Non Adressable GB state
	//**********************************************************************************
	boolean GetHalt();
	void SetHalt(boolean state);
	
	
	//**********************************************************************************
	//								Register Access Methods
	//**********************************************************************************
	
	//flag bits get/set methods
	public enum flag {
		Z, N, H, C
	}
	
	boolean getFlag(flag flagBit);
	void setFlag(flag flagBit, boolean value);
	
	//Get the current ROM byte and auto-increment SP
	int getRomByte();
	
	/*Get/Set 16 bit register given by two bit constant vv/qq where we have
	 * vv: 0 - BC, 1 - DE, 2 - HL, 3 - SP
	 * qq: 0 - BC, 1 - DE, 2 - HL, 3 - AF
	 */
	int readRegisterVV(int vv);
	int readRegisterQQ(int qq);
	
	void writeRegisterVV(int vv, int value);
	void writeRegisterQQ(int qq, int value);
	
	//Get/set 16 bit register pairs with enum
	public enum regPair {
		AF, BC, DE, HL, SP, PC
	}
	
	void setRegPair(regPair register, int value);
	int getRegPair(regPair register);
	
	/*Get/Set register u, where u is a 3 bit constant and, u: 0 - B, 1 - C, 2 - D,
	 * 3 - E, 4 - H, 5 - L, 6 - *(HL), 7 - A
	 */
	int readRegisterU(int u);
	
	void writeRegisterU(int u, int value);
	
	//get/set 8 bit register with enum
	public enum reg {
		A, F, B, C, D, E, H, L
	}
	
	void setReg(reg register, int value);
	int getReg(reg register);
	
	//master interrupt enable register
	void setMIE(boolean value);
	boolean getMIE();
	
	//individual interrupt flags
	public enum InterruptFlag {
		blank, LCDC, TMR, Serial, BTN
	}
	
	//individual interrupt enable flags
	void setIE(InterruptFlag interrupt, boolean value);
	boolean getIE(InterruptFlag interrupt);
	
	//individual interrupt request flags
	void setIR(InterruptFlag interrupt, boolean value);
	boolean getIR(InterruptFlag interrupt);
	
	//**********************************************************************************
	//							Mapped Memory Access Methods
	//**********************************************************************************
	//MappedMemory methods handle all writes/reads to memory mapped memory/registers/peripherals/MBC
	
	void writeMappedMemory(int addr, int value);
	
	int readMappedMemory(int addr);
	
	//**********************************************************************************
	//								Miscellaneous Methods
	//**********************************************************************************
	
	void attachInput(InputInterface mappedButtons);
	
	String getRomName();
	
	void gbDMA(int DMAOffset);
}