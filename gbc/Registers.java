package gbc;

import java.io.Serializable;

public class Registers implements Serializable {
	int a, f, b, c, d, e, h, l, pc, sp;
	boolean mie, blankIE, blankIR, lcdcIE, lcdcIR, tmrIE, tmrIR, serIE, serIR, btnIE, btnIR, halted;
	
	public Registers(boolean color) {
		//common initial values
		//XXX no sources give initial value of MIE
		mie = false;
		
		//Register initialisation
		initRegs(color);
	}
	
	private void initRegs(boolean color) {
		a = color ? 0x11 : 0x01;
		f = 0xB0;
		b = 0x00;
		c = 0x13;
		d = 0x00;
		e = 0xD8;
		h = 0x01;
		l = 0x4D;
		sp = 0xFFFE;
	}
}
