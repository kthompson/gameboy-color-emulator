package gbc;

public class Rtc {
	int seconds, minutes, hours, days;
	boolean halt, carry;
	
	int getDays() {
		return days;
	}
	
	int getHours(){
		return hours;
	}
	
	int getMinutes() {
		return minutes;
	}
	
	int getSeconds() {
		return seconds;
	}
	
	void setHalt(boolean value) {
		halt = value;
	}
	
	boolean getHalt() {
		return halt;
	}
	
	void setCarry(boolean value) {
		carry = value;
	}
	
	boolean getCarry() {
		return carry;
	}
	
	void setDays(int value) {
		days = value;
	}
	
	void setHours(int value) {
		hours = value;
	}
	
	void setMinutes(int value) {
		minutes = value;
	}
	
	void setSeconds(int value) {
		seconds = value;
	}
	
	void advanceSecond() {
		if (halt == false) {
			if (++seconds == 60) {
				seconds = 0;
				if (++minutes == 60) {
					minutes = 0;
					if (++hours == 24) {
						hours = 0;
						if (++days == 0x200) {
							carry = true;
							days = 0;
						}
					}
				}
			}
		}		
	}
}
