package gbc;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.glfw.GLFWKeyCallback;


public class Input implements InputInterface {
	long window;
	public GLFWKeyCallback   keyCallback;
	
	public boolean closeFlag, playFlag, stepFlag, frameFlag;
	public boolean a, b, start, select, up, down, left, right, saveState, loadState;
	
	public Input(Memory gbMemory) {
		keyCallback = new GLFWKeyCallback() {
			public void invoke(long window, int key, int scancode, int action, int mods) {
				keyCallbackFcn(window, key, scancode, action, mods);
			}
		};
		
		//set buttons to initially be released
		gbMemory.writeMappedMemory(0xFF00, 0x0F);
	}

	public void keyCallbackFcn(long window, int key, int scancode, int action, int mods) {
		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_ESCAPE)	closeFlag = true;
			
			else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) playFlag = !playFlag;
			
			else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) stepFlag = true;
			
			else if (key == GLFW_KEY_F3 && action == GLFW_PRESS) frameFlag = true;
			
			//default key bindings
			else if (key == GLFW_KEY_W) up = true;
			else if (key == GLFW_KEY_A) left = true;
			else if (key == GLFW_KEY_S) down = true;
			else if (key == GLFW_KEY_D) right = true;
			else if (key == GLFW_KEY_J) a = true;
			else if (key == GLFW_KEY_K) b = true;
			else if (key == GLFW_KEY_U) select = true;
			else if (key == GLFW_KEY_I) start = true;
			else if (key == GLFW_KEY_F5) saveState = true;
			else if (key == GLFW_KEY_F7) loadState = true;
		}
		
		else if (action == GLFW_RELEASE) {
			if (key == GLFW_KEY_W) up = false;
			else if (key == GLFW_KEY_A) left = false;
			else if (key == GLFW_KEY_S) down = false;
			else if (key == GLFW_KEY_D) right = false;
			else if (key == GLFW_KEY_J) a = false;
			else if (key == GLFW_KEY_K) b = false;
			else if (key == GLFW_KEY_U) select = false;
			else if (key == GLFW_KEY_I) start = false;
			else if (key == GLFW_KEY_F5) saveState = false;
			else if (key == GLFW_KEY_F7) loadState = false;			
		}
	}
	
	@Override
	public void attachToWindow(long GLFWWindow) {
		window = GLFWWindow;
	}

	@Override
	public void setKeys() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateButtons() {
		// TODO Auto-generated method stub
		
	}

	public void closeInput() {
        keyCallback.release();
	}

	@Override
	public int getKeys(int currentVal) {
		int buttons = 0x0F;
		if ((currentVal & 0x30) == 0x10) {
			buttons ^= a ? 0x01 : 0;
			buttons ^= b ? 0x02 : 0;
			buttons ^= select ? 0x04 : 0;
			buttons ^= start ? 0x08 : 0;
		}
		
		else if ((currentVal & 0x30) == 0x20) {
			buttons ^= right ? 0x01 : 0;
			buttons ^= left ? 0x02 : 0;
			buttons ^= up ? 0x04 : 0;
			buttons ^= down ? 0x08 : 0;
		}
		
		return (0xC0 | (0x30 & currentVal) | (0x0F & buttons));
	}
}
