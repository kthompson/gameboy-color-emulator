package gbc;

//Import enums
import gbc.MemoryInterface.*;

public class CPU {
		
	static int executeNextOpCode(Memory gbMemory) {
		int opcode = gbMemory.getRomByte();
		
		//TODO remove
		/*if (opcode == 0x73) {
			System.out.println(Integer.toHexString(gbMemory.getRegPair(regPair.PC)));
		}*/
		
		//NOP
		if (opcode == 0) {
			return 1;
		}
		
		//Load 16 bit constant into VV
		else if ((opcode & 0b11001111) == 0x01) {
			int M = gbMemory.getRomByte();
			int N = gbMemory.getRomByte();

			int vv = (opcode & 0b00110000) >>> 4;
			gbMemory.writeRegisterVV(vv, N << 8 | M);
			return 3;
		}
		
		else if (opcode == 0x02) {
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.BC), gbMemory.getReg(reg.A));
			return 2;
		}
		
		//Increment VV
		else if ((opcode & 0b11001111) == 0x03) {
			int vv = (opcode & 0b00110000) >>> 4;
			int value = gbMemory.readRegisterVV(vv) + 1;
			gbMemory.writeRegisterVV(vv, value);
			return 2;
		}
		
		//Increment u
		else if ((opcode & 0b11000111) == 0x04) {
			int u = (opcode & 0b00111000) >>> 3;
			int value = gbMemory.readRegisterU(u) + 1;
			gbMemory.writeRegisterU(u, value);
			
			gbMemory.setFlag(flag.Z, value == 0 || value == 0x100);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.H, (value & 0x0F) == 0);
			
			if (u == 0b110) return 3;
			else return 1;
		}
		
		//Decrement u
		else if ((opcode & 0b11000111) == 0x05) {
			int u = (opcode & 0b00111000) >>> 3;
			int value = gbMemory.readRegisterU(u) - 1;
			gbMemory.writeRegisterU(u, value);
			
			gbMemory.setFlag(flag.Z, value == 0);
			gbMemory.setFlag(flag.N, true);
			gbMemory.setFlag(flag.H, (value & 0x0F) == 0x0F);
			
			if (u == 0b110) return 3;
			else return 1;
		}
		
		//Load constant into u
		else if ((opcode & 0b11000111) == 0x06) {
			int u = (opcode & 0b00111000) >>> 3;
			int N = gbMemory.getRomByte();
			gbMemory.writeRegisterU(u, N);
			
			if (u == 0b110) return 3;
			return 2;
		}
		
		//rotate left and copy bit 7 into carry
		else if (opcode == 0x07) {
			if ((gbMemory.getReg(reg.A) & 0x80) == 0x80) gbMemory.setFlag(flag.C, true);
			else gbMemory.setFlag(flag.C, false);
			
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, false);
			
			gbMemory.setReg(reg.A, ((gbMemory.getReg(reg.A) << 1) | (gbMemory.getReg(reg.A) >>> 7)) & 0xFF);
			return 1;
		}
		
		else if (opcode == 0x08) {
			int addrl = gbMemory.getRomByte();
			int addrh = gbMemory.getRomByte();
			gbMemory.writeMappedMemory(addrh << 8 | addrl, gbMemory.getRegPair(regPair.SP) & 0x00FF);
			
			if (addrl == 0xFF) {
				addrl = 0;
				addrh = (addrh == 0xFF) ? 0x00 : addrh + 1;
			}
			
			else addrl++;
			
			gbMemory.writeMappedMemory(addrh << 8 | addrl, (gbMemory.getRegPair(regPair.SP) & 0xFF00) >>> 8);
			
			return 5;
		}
		
		else if ((opcode & 0b11001111) == 0x09) {
			int HL = gbMemory.getRegPair(regPair.HL);
			int sum = gbMemory.readRegisterVV((opcode & 0b00110000) >>> 4);
			if ((HL & 0xFFF) + (sum & 0xFFF) > 0xFFF) {
				gbMemory.setFlag(flag.H, true);
			}
			else gbMemory.setFlag(flag.H, false);
			
			HL = HL + sum;
			if (HL > 0xFFFF) {
				HL -= 0x10000;
				gbMemory.setFlag(flag.C, true);
			}
			else gbMemory.setFlag(flag.C, false);
			
			gbMemory.setFlag(flag.N, false);
			
			gbMemory.setRegPair(regPair.HL, HL);
			return 2;
		}
		
		else if (opcode == 0x0A) {
			gbMemory.setReg(reg.A, gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.BC)));
			return 2;
		}
		
		else if ((opcode & 0b11001111) == 0x0B) {
			int vv = (opcode & 0b00110000) >>> 4;
			int value = gbMemory.readRegisterVV(vv) - 1;
			gbMemory.writeRegisterVV(vv, value);
			return 2;
		}
		
		else if (opcode == 0x0F) {
			if ((gbMemory.getReg(reg.A) & 0x01) == 0x01) gbMemory.setFlag(flag.C, true);
			else gbMemory.setFlag(flag.C, false);
			
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, false);
			
			gbMemory.setReg(reg.A, ((gbMemory.getReg(reg.A) >>> 1) | (gbMemory.getReg(reg.A) << 7)) & 0xFF);
			return 1;
		}
		
		else if (opcode == 0x10) {
			//Increment spare byte
			gbMemory.setRegPair(regPair.PC, gbMemory.getRegPair(regPair.PC) + 1);
			
			//TODO Implement STOP instruction here. Stop oscillator circuit and LCD controller. Only broken by reset
			System.out.println("STOP instruction. PC - " + Integer.toHexString(gbMemory.getRegPair(regPair.PC)));
			
			
			
			//XXX do I need to add IE, IF, P1 checks?
			//check for oscillator mode switch
			if ((gbMemory.readMappedMemory(0xFF4D) & 0x01) == 0x01) {
				gbMemory.writeMappedMemory(0xFF4D, gbMemory.readMappedMemory(0xFF4D) ^ 0x81);
			}
			System.out.println(Integer.toHexString(gbMemory.readMappedMemory(0xFF4D)));
			
			return 1;
		}
		
		else if (opcode == 0x12) {
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.DE), gbMemory.getReg(reg.A));
			return 2;
		}
		
		else if (opcode == 0x17) {
			boolean tempCarry = gbMemory.getFlag(flag.C);
			int A = gbMemory.getReg(reg.A);
			
			if ((A & 0x80) == 0x80) gbMemory.setFlag(flag.C, true);
			else gbMemory.setFlag(flag.C, false);
			
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, false);
			
			A = (A << 1) & 0xFF;
			if (tempCarry) A = A | 1;
			
			gbMemory.setReg(reg.A, A);
			return 1;
		}
		
		else if (opcode == 0x18) {
			byte jump = (byte) gbMemory.getRomByte();
			gbMemory.setRegPair(regPair.PC, gbMemory.getRegPair(regPair.PC) + jump);
			return 3;
		}
		
		else if (opcode == 0x1A) {
			gbMemory.setReg(reg.A, gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.DE)));
			return 2;
		}
		
		else if (opcode == 0x1F) {
			boolean tempCarry = gbMemory.getFlag(flag.C);
			int A = gbMemory.getReg(reg.A);
			
			if ((A & 0x01) == 0x01) gbMemory.setFlag(flag.C, true);
			else gbMemory.setFlag(flag.C, false);
			
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, false);
			
			A = (A >>> 1);
			if (tempCarry) A = A | 0x80;
			
			gbMemory.setReg(reg.A, A);
			return 1;
		}
		
		else if ((0b11100111 & opcode) == 0x20) {
			boolean condition = jumpCondition((0b00011000 & opcode) >>> 3, gbMemory);
			byte jump = (byte) gbMemory.getRomByte();
			if (condition) gbMemory.setRegPair(regPair.PC, gbMemory.getRegPair(regPair.PC) + jump);
			return condition ? 3 : 2;
		}
		
		else if (opcode == 0x22) {
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.HL), gbMemory.getReg(reg.A));
			gbMemory.setRegPair(regPair.HL, gbMemory.getRegPair(regPair.HL) + 1);
			return 2;
		}
		
		else if (opcode == 0x27) {
			int a = gbMemory.getReg(reg.A);
			int f = gbMemory.getReg(reg.F);
			
			if (!((f & 0x40) == 0x40)) {
				if (((f & 0x20) == 0x20) || (a & 0x0F) > 9)
					a += 6;
				
				if (((f & 0x10) == 0x10) || a > 0x9F)
					a += 0x60;
			}
			
			else {
				if ((f & 0x20) == 0x20){
					a = a-6;
					if (a < 0) a += 0x1000;
				}
				
				if ((f & 0x10) == 0x10) {
					a -= 0x60;
					if (a < 0) a += 0x1000;
				}
			}
			
			f = 0;
			if (gbMemory.getFlag(flag.Z)) f |= 0x80;
			if (gbMemory.getFlag(flag.H)) f |= 0x20;
			f &= ~f;
			f = f & 0xFF;
			
			if ((a & 0x100) == 0x100)
				f |= 0x10;
			
			a &= 0xFF;
			if (a == 0)
				f |= 0x80;
			
			gbMemory.setReg(reg.F, f);
			
			//System.out.println("A - " + Integer.toHexString(a) + " , F - " + Integer.toBinaryString(gbMemory.getReg(reg.F)));
			
			/*if (gbMemory.getFlag(flag.N)) {
				if (gbMemory.getFlag(flag.H)) {
					a = (a - 6);
					if (a < 0) a += 0x100;
				}
				
				if (gbMemory.getFlag(flag.C)) a -= 0x60;
			}
			
			else {
				if (gbMemory.getFlag(flag.H) || (a & 0x0F) > 9) a+=6;
				
				if (gbMemory.getFlag(flag.C) || a > 0x9F) a+=0x60;
			}
					
			gbMemory.setFlag(flag.C, a > 0xFF || a <  0);
			if (a < 0) a += 0x100;
			a = a & 0xFF;
			gbMemory.setFlag(flag.Z, a == 0);
			gbMemory.setFlag(flag.H, false);*/

			gbMemory.setReg(reg.A, a);
			return 1;
		}
		
		else if (opcode == 0x2A) {
			gbMemory.setReg(reg.A, gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.HL)));
			gbMemory.setRegPair(regPair.HL, gbMemory.getRegPair(regPair.HL) + 1);
			return 2;
		}
		
		else if (opcode == 0x2F) {
			gbMemory.setFlag(flag.H, true);
			gbMemory.setFlag(flag.N, true);
			gbMemory.setReg(reg.A, (~gbMemory.getReg(reg.A)) & 0xFF);
			return 1;
		}
		
		else if (opcode == 0x32) {
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.HL), gbMemory.getReg(reg.A));
			gbMemory.setRegPair(regPair.HL, gbMemory.getRegPair(regPair.HL) - 1);
			return 2;
		}
		
		//set carry flag
		else if (opcode == 0x37) {
			gbMemory.setFlag(flag.C, true);
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			return 1;
		}
		
		else if (opcode == 0x3A) {
			gbMemory.setReg(reg.A, gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.HL)));
			gbMemory.setRegPair(regPair.HL, gbMemory.getRegPair(regPair.HL) - 1);
			return 2;	
		}
		
		//Complement carry flag		
		else if (opcode == 0x3F) {
			gbMemory.setFlag(flag.C, !gbMemory.getFlag(flag.C));
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			return 1;
		}
		
		//halt instruction
		else if (opcode == 0x76) {
			gbMemory.SetHalt(true);
			return 1;
		}
		
		//Oooh yeah, look at those 64 instructions dealt with in one fell swoop. So sweet, so lovely, so easy
		else if ((opcode & 0b11000000) == 0x40) {
			int u = opcode & 0b00000111;
			int v = (opcode & 0b00111000) >>> 3;
			gbMemory.writeRegisterU(v, gbMemory.readRegisterU(u));
			
			if (u == 0b110 || v == 0b110) return 2;
			return 1;
		}
		
		//8 bit addition without carry
		else if ((opcode & 0b11111000) == 0x80) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			int result = A + uVal;
			
			//set flag bits
			gbMemory.setFlag(flag.H, (uVal & 0x0F) + (A & 0x0F) > 0x0F);
			gbMemory.setFlag(flag.C, result > 0xFF);
			gbMemory.setFlag(flag.Z, result == 0 || result == 0x100);
			gbMemory.setFlag(flag.N, false);
			
			gbMemory.setReg(reg.A, result);
			
			if (opcode == 0x86) return 2;
			else return 1;
		}
		
		//8 bit addition with carry
		else if ((opcode & 0b11111000) == 0x88) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			int carry = gbMemory.getFlag(flag.C) ? 1 : 0;
			int result = A + uVal + carry;
			
			//set flag bits
			gbMemory.setFlag(flag.H, (uVal & 0x0F) + (A & 0x0F) + carry > 0x0F);
			gbMemory.setFlag(flag.C, result > 0xFF);
			gbMemory.setFlag(flag.Z, result == 0 || result == 0x100);
			gbMemory.setFlag(flag.N, false);
			
			gbMemory.setReg(reg.A, result);
			
			if (opcode == 0x8E) return 2;
			else return 1;
		}
		
		//8 bit subtraction without carry
		else if ((opcode & 0b11111000) == 0x90) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			int result = A - uVal;
			
			//set flag bits
			gbMemory.setFlag(flag.H, (A & 0x0F) - (uVal & 0x0F) < 0);
			gbMemory.setFlag(flag.C, result < 0);
			gbMemory.setFlag(flag.Z, result == 0);
			gbMemory.setFlag(flag.N, true);
			
			gbMemory.setReg(reg.A, result);
			
			if (opcode == 0x96) return 2;
			else return 1;
		}
		
		//8 bit subtraction with carry
		else if ((opcode & 0b11111000) == 0x98) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			int carry = gbMemory.getFlag(flag.C) ? 1 : 0;
			int result = A - uVal - carry;
			
			//set flag bits
			gbMemory.setFlag(flag.H, (A & 0x0F) - (uVal & 0x0F) - carry < 0);
			gbMemory.setFlag(flag.C, result < 0);
			gbMemory.setFlag(flag.Z, result == 0 || result == -0x100);
			gbMemory.setFlag(flag.N, true);
			
			gbMemory.setReg(reg.A, result);
			
			if (opcode == 0x9E) return 2;
			else return 1;
		}
		
		//8 bit and operation
		else if ((opcode & 0b11111000) == 0xA0) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			A = A & uVal;
			
			//set flag bits
			gbMemory.setFlag(flag.C, false);
			gbMemory.setFlag(flag.H, true);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, A == 0);
			
			gbMemory.setReg(reg.A, A);
			
			if (opcode == 0xA6) return 2;
			else return 1;
		}
		
		//8 bit xor operation
		else if ((opcode & 0b11111000) == 0xA8) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			A = A ^ uVal;
			
			//set flag bits
			gbMemory.setFlag(flag.C, false);
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, A == 0);
			
			gbMemory.setReg(reg.A, A);
			
			if (opcode == 0xAE) return 2;
			else return 1;
		}
		
		//8 bit or operation
		else if ((opcode & 0b11111000) == 0xB0) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			A = A | uVal;
			
			//set flag bits
			gbMemory.setFlag(flag.C, false);
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, A == 0);
			
			gbMemory.setReg(reg.A, A);
			
			if (opcode == 0xB6) return 2;
			else return 1;
		}
		
		//compare
		else if ((opcode & 0b11111000) == 0xB8) {
			int u = opcode & 0b00000111;
			int uVal = gbMemory.readRegisterU(u);
			int A = gbMemory.getReg(reg.A);
			int result = A - uVal;
			
			//set flag bits
			gbMemory.setFlag(flag.C, result < 0);
			gbMemory.setFlag(flag.H, (A & 0x0F) - (uVal & 0x0F) < 0);
			gbMemory.setFlag(flag.N, true);
			gbMemory.setFlag(flag.Z, result == 0);
			
			if (opcode == 0xBE) return 2;
			else return 1;
		}
		
		//return if condition is true
		else if ((opcode & 0b11100111) == 0xC0) {
			int cc = (opcode & 0b00011000) >>> 3;
			boolean condition = jumpCondition(cc, gbMemory);
			
			if (condition) {
				int pcLow = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
				gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
				int pcHigh = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
				gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
				
				gbMemory.setRegPair(regPair.PC, (pcHigh << 8) | pcLow);
				return 5;
			}
			
			return 2;
		}
		
		//pop 16 bit register qq
		else if ((opcode & 0b11001111) == 0xC1) {
			int qq = (opcode & 0b00110000) >>> 4;
			int qqLow = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
			int qqHigh = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
			
			gbMemory.writeRegisterQQ(qq, (qqHigh << 8) | qqLow);
			
			return 3;
		}
		
		//conditional jump instructions
		else if ((opcode & 0b11100111) == 0xC2) {
			int cc = (opcode & 0b00011000) >>> 3;
			boolean condition = jumpCondition(cc, gbMemory);
			
			int N = gbMemory.getRomByte();
			int M = gbMemory.getRomByte();
			
			if (condition) {
				gbMemory.setRegPair(regPair.PC, (M << 8) | N);
				return 4;
			}
			
			else return 3;
		}
		
		//Jump to literal address
		else if (opcode == 0xC3) {
			int N = gbMemory.getRomByte();
			int M = gbMemory.getRomByte();
			gbMemory.setRegPair(regPair.PC, (M << 8) | N);
			return 4;
		}
		
		//Conditional call instruction
		else if ((opcode & 0b11100111) == 0xC4) {
			int cc = (opcode & 0b00011000) >>> 3;
			boolean condition = jumpCondition(cc, gbMemory);
			
			int N = gbMemory.getRomByte();
			int M = gbMemory.getRomByte();
			
			if (condition) {
				gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
				gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), (gbMemory.getRegPair(regPair.PC) >>> 8 ) & 0xFF);
				gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
				gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), gbMemory.getRegPair(regPair.PC) & 0xFF);
				gbMemory.setRegPair(regPair.PC, (M << 8) | N);
				return 6;
			}
			
			else return 3;
		}
		
		//push 16 bit register qq 
		else if ((opcode & 0b11001111) == 0xC5) {
			int qq = (opcode & 0b00110000) >>> 4;
			int qqVal = gbMemory.readRegisterQQ(qq);
			
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), (qqVal >>> 8) & 0xFF);
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), qqVal & 0xFF);			
			
			return 4;
		}
		
		//Add 8 bit constant to A
		else if (opcode == 0xC6) {
			int N = gbMemory.getRomByte();
			int A = gbMemory.getReg(reg.A);
			int result = A + N;
			
			//set flag bits
			gbMemory.setFlag(flag.C, result > 0xFF);
			gbMemory.setFlag(flag.H, (N & 0x0F) + (A & 0x0F) > 0x0F);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, result == 0 || result == 0x100);
			
			gbMemory.setReg(reg.A, result);
			
			return 2;
		}
		
		//Reset to one of 8 locations on first ROM page
		else if ((opcode & 0b11000111) == 0xC7) {
			int t = (opcode & 0b00111000) >>> 3;
			
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), (gbMemory.getRegPair(regPair.PC) >>> 8 ) & 0xFF);
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), gbMemory.getRegPair(regPair.PC) & 0xFF);
			
			//Write the reset location
			gbMemory.setRegPair(regPair.PC, t*8);
			
			return 4;
		}
		
		//return
		else if (opcode == 0xC9) {
			int pcLow = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
			int pcHigh = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
			
			gbMemory.setRegPair(regPair.PC, (pcHigh << 8) | pcLow);
			
			return 4;
		}
		
		else if (opcode == 0xCB) {
			opcode = gbMemory.getRomByte();
			int u = (opcode & 0b00111000) >>> 3;
			int v = (opcode & 0b00000111);
			
			//bit rotate/shift instructions
			if ((0b11000000 & opcode) == 0x00) {
				int vReg = gbMemory.readRegisterU(v);
				
				//rotate left with bit 7 copied into carry
				if ((0b00111000 & opcode) == 0x00) {
					if ((vReg & 0x80) == 0x80) {
						vReg = (vReg << 1) & 0xFF;
						vReg = vReg | 0x01;
						gbMemory.setFlag(flag.C, true);
					}
					
					else {
						vReg = vReg << 1;
						gbMemory.setFlag(flag.C, false);
					}
				}
				
				//rotate right with bit 0 copied into carry
				else if ((0b00111000 & opcode) == 0x08) {
					if ((vReg & 0x01) == 0x01) {
						vReg = vReg >>> 1;
						vReg = vReg | 0x80;
						gbMemory.setFlag(flag.C, true);
					}
					
					else {
						vReg = vReg >>> 1;
						gbMemory.setFlag(flag.C, false);
					}
				}
				
				//rotate left through carry
				else if ((0b00111000 & opcode) == 0x10) {
					vReg = vReg << 1;
					
					if (gbMemory.getFlag(flag.C)) vReg = vReg | 0x01;
					if ((vReg & 0x100) == 0x100) gbMemory.setFlag(flag.C, true);
					else gbMemory.setFlag(flag.C, false);
					
					vReg = vReg & 0xFF;
				}
				
				//rotate right through carry
				else if ((0b00111000 & opcode) == 0x18) {					
					if (gbMemory.getFlag(flag.C)) vReg = vReg | 0x100;
					if ((vReg & 0x01) == 0x01) gbMemory.setFlag(flag.C, true);
					else gbMemory.setFlag(flag.C, false);
					vReg = vReg >>> 1;
				}
				
				//shift left into carry
				else if ((0b00111000 & opcode) == 0x20) {
					gbMemory.setFlag(flag.C, (vReg & 0x80) == 0x80);
					vReg = (vReg << 1) & 0xFF;
				}
				
				//shift right into carry with bit 7 unchanged
				else if ((0b00111000 & opcode) == 0x28) {
					if ((vReg & 0x80) == 0x80) vReg += 0x100;
					gbMemory.setFlag(flag.C, (vReg & 0x01) == 0x01);
					vReg = vReg >>> 1;
				}
				
				//swap nibbles
				else if ((0b00111000 & opcode) == 0x30) {
					vReg = ((vReg & 0x0F) << 4) | ((vReg & 0xF0) >>> 4);
					gbMemory.setFlag(flag.C, false);
				}
				
				//shift right into carry with bit 7 reset
				else if ((0b00111000 & opcode) == 0x38) {
					gbMemory.setFlag(flag.C, (vReg & 0x01) == 0x01);
					vReg = vReg >>> 1;
				}
				
				gbMemory.setFlag(flag.Z, vReg == 0);
				gbMemory.setFlag(flag.N, false);
				gbMemory.setFlag(flag.H, false);
				gbMemory.writeRegisterU(v, vReg);
				
				if (v == 0b110) return 4;
				return 2;
			}
			
			//bit test
			else if ((0b11000000 & opcode) == 0x40) {
				gbMemory.setFlag(flag.H, true);
				gbMemory.setFlag(flag.N, false);
				gbMemory.setFlag(flag.Z, (gbMemory.readRegisterU(v) & (0x01 << u)) == 0);
				
				if (v == 0b110) return 3;
				return 2;
			}
			
			//bit set or reset
			else if ((0b10000000 & opcode) == 0x80) {
				int vReg = gbMemory.readRegisterU(v);
				
				if ((0b01000000 & opcode) == 0x00) vReg = vReg & (~(1 << u) & 0xFF);
				else vReg = vReg | (1 << u);
				
				gbMemory.writeRegisterU(v, vReg);
				
				if (v == 0b110) return 4;
				return 2;
			}
			
			//Not going to lie. Huge number of different cases but no flags to worry about. It's not that bad
			return -1;
		}
		
		//call literal address
		else if (opcode == 0xCD) {
			int N = gbMemory.getRomByte();
			int M = gbMemory.getRomByte();
			
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), (gbMemory.getRegPair(regPair.PC) >>> 8 ) & 0xFF);
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), gbMemory.getRegPair(regPair.PC) & 0xFF);
			
			gbMemory.setRegPair(regPair.PC, (M << 8) | N);
			
			return 6;
		}
		
		//Add 8 bit constant and carry to A
		else if (opcode == 0xCE) {
			int N = gbMemory.getRomByte();
			int A = gbMemory.getReg(reg.A);
			int carry = gbMemory.getFlag(flag.C) ? 1 : 0;
			int result = A + N + carry;
			
			//set flag bits
			gbMemory.setFlag(flag.C, result > 0xFF);
			gbMemory.setFlag(flag.H, (N & 0x0F) + (A & 0x0F) + carry > 0x0F);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, result % 0x100 == 0);
			
			gbMemory.setReg(reg.A, result);
			
			return 2;
		}
		
		//subtract literal from A
		else if (opcode == 0xD6) {
			int N = gbMemory.getRomByte();
			int A = gbMemory.getReg(reg.A);
			int result = A - N;
			
			//set flag bits
			gbMemory.setFlag(flag.C, result < 0);
			gbMemory.setFlag(flag.H, (A & 0x0F) - (N & 0x0F) < 0);
			gbMemory.setFlag(flag.N, true);
			gbMemory.setFlag(flag.Z, result == 0);
			
			gbMemory.setReg(reg.A, result);
			
			return 2;
		}

		//return from interrupt
		else if (opcode == 0xD9) {
			int pcLow = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
			int pcHigh = gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP));
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) + 1);
			
			gbMemory.setRegPair(regPair.PC, (pcHigh << 8) | pcLow);
			gbMemory.setMIE(true);		//set the MIE flag to true
			
			return 4;
		}
		
		//subtract literal from A with carry
		else if ( opcode == 0xDE) {
			int N = gbMemory.getRomByte();
			int A = gbMemory.getReg(reg.A);
			int carry = gbMemory.getFlag(flag.C) ? 1 : 0;
			int result = A - N - carry;
			
			//set flag bits
			gbMemory.setFlag(flag.C, result < 0);
			gbMemory.setFlag(flag.H, (A & 0x0F) - (N & 0x0F) - carry < 0);
			gbMemory.setFlag(flag.N, true);
			gbMemory.setFlag(flag.Z, result % 0x100 == 0);
			
			gbMemory.setReg(reg.A, result);
			
			return 2;
		}
		
		//Load A into 0xFF00 + N
		else if (opcode == 0xE0) {
			int N = gbMemory.getRomByte();
			int addr = 0xFF00 + N;
			
			gbMemory.writeMappedMemory(addr, gbMemory.getReg(reg.A));
			
			return 3;
		}
		
		//Load A into 0xFF00 + C
		else if (opcode == 0xE2) {
			int C = gbMemory.getReg(reg.C);
			int addr = 0xFF00 + C;
			
			gbMemory.writeMappedMemory(addr, gbMemory.getReg(reg.A));
			
			return 2;
		}
		
		//bitwise & of A and literal
		else if (opcode == 0xE6) {
			int N = gbMemory.getRomByte();
			int A = gbMemory.getReg(reg.A);
			A = A & N;
			
			//set flag bits
			gbMemory.setFlag(flag.C, false);
			gbMemory.setFlag(flag.H, true);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, A == 0);
			
			gbMemory.setReg(reg.A, A);
			
			return 2;
		}
		
		else if (opcode == 0xE8) {
			int eu = gbMemory.getRomByte();
			byte e = (byte) eu;
			
			int SP = gbMemory.getRegPair(regPair.SP);
			
			gbMemory.setFlag(flag.C, (SP & 0xFF) + eu > 0xFF);
			gbMemory.setFlag(flag.H, (SP & 0x0F) + (eu & 0x0F) > 0x0F);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, false);

			gbMemory.setRegPair(regPair.SP, SP + e);
			return 4;
		}
		
		//Load HL into PC
		else if (opcode == 0xE9) {
			gbMemory.setRegPair(regPair.PC, gbMemory.getRegPair(regPair.HL));
			return 1;
		}
		
		//Load A into memory at address MN
		else if (opcode == 0xEA) {
			int N = gbMemory.getRomByte();
			int M = gbMemory.getRomByte();
			
			gbMemory.writeMappedMemory((M << 8) | N, gbMemory.getReg(reg.A));
			
			return 4;
		}
		
		//bitwise xor of A and literal
		else if (opcode == 0xEE) {
			int N = gbMemory.getRomByte();
			int result = gbMemory.getReg(reg.A) ^ N;
			gbMemory.setReg(reg.A, result);
			
			gbMemory.setFlag(flag.C, false);
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, result == 0);
			
			return 2;
		}
		
		//Load memory at 0xFF00 + N into A
		else if (opcode == 0xF0) {
			int N = gbMemory.getRomByte();
			int addr = 0xFF00 + N;
			
			if (N == 0) {
				N = 0;
			}
			
			gbMemory.setReg(reg.A, gbMemory.readMappedMemory(addr));
			
			return 3;
		}
		
		//Load memory at 0xFF00 + C into A
		else if (opcode == 0xF2) {
			int C = gbMemory.getReg(reg.C);
			int addr = 0xFF00 + C;
			
			gbMemory.setReg(reg.A, gbMemory.readMappedMemory(addr));
			
			return 2;
		}
		
		//Disable interrupt
		else if (opcode == 0xF3) {
			//technically, the interrupt is only disabled after the end of the next instruction but
			//interrupts aren't regular so we can disable it right now.
			gbMemory.setMIE(false);
			return 1;
		}
		
		//bitwise or of A and literal
		else if (opcode == 0xF6) {
			int N = gbMemory.getRomByte();
			int A = gbMemory.getReg(reg.A);
			A = A | N;
			
			//set flag bits
			gbMemory.setFlag(flag.C, false);
			gbMemory.setFlag(flag.H, false);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, A == 0);
			
			gbMemory.setReg(reg.A, A);
			
			return 2;
		}
		
		//Load
		else if (opcode == 0xF8) {
			int eu = gbMemory.getRomByte();
			byte e = (byte) eu;
			
			int SP = gbMemory.getRegPair(regPair.SP);
			
			gbMemory.setFlag(flag.C, (SP & 0xFF) + eu > 0xFF);
			gbMemory.setFlag(flag.H, (SP & 0x0F) + (eu & 0x0F) > 0x0F);
			gbMemory.setFlag(flag.N, false);
			gbMemory.setFlag(flag.Z, false);
						
			gbMemory.setRegPair(regPair.HL, SP + e);
			return 3;
		}
		
		//Load HL into SP
		else if (opcode == 0xF9) {
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.HL));
			return 2;
		}
		
		//Load memory at address MN into A
		else if (opcode == 0xFA) {
			int N = gbMemory.getRomByte();
			int M = gbMemory.getRomByte();
			
			gbMemory.setReg(reg.A, gbMemory.readMappedMemory((M << 8) | N));
			return 4;
		}
		
		//Enable interrupts
		else if (opcode == 0xFB) {
			gbMemory.setMIE(true);
			return 1;
		}
		
		//Compare A with literal
		else if (opcode == 0xFE) {
			int N = gbMemory.getRomByte();
			int A = gbMemory.getReg(reg.A);
			int result = A - N;
			
			//set flag bits
			gbMemory.setFlag(flag.C, result < 0);
			gbMemory.setFlag(flag.H, (A & 0x0F) - (N & 0x0F) < 0);
			gbMemory.setFlag(flag.N, true);
			gbMemory.setFlag(flag.Z, result == 0);
			return 2;
		}
		
		else {
			//If no opcode was found
			System.out.println("mbc[0] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[0]) +
					"\tmbc[1] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[1]));
			System.out.println("mbc[2] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[2]) +
					"\tmbc[3] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[3]) + "\n");
			throw new UnsupportedOperationException("Opcode not found: " + opcode + " at address " + Integer.toHexString(gbMemory.getRegPair(regPair.PC)));
		}
	}

	static boolean jumpCondition(int check, Memory gbMemory) {
		switch (check) {
		case 0: return (gbMemory.getFlag(flag.Z) == false);
		case 1: return (gbMemory.getFlag(flag.Z) == true);
		case 2: return (gbMemory.getFlag(flag.C) == false);
		case 3: return (gbMemory.getFlag(flag.C) == true);
		default: throw new UnsupportedOperationException("Condition not available: " + check);
		}
	}
}
