package gbc;

public class Sprite {
	Memory gbMemory;
	int xPos, yPos, height, tile, attribute, color, index, colorPalette, charBank, dmgPalette;
	boolean colorFlag, flipx, flipy, bgPriority, largeBlock, visible;
	
	public Sprite(Memory memoryRef, boolean colorVal, int i) {
		gbMemory = memoryRef;
		yPos = gbMemory.readMappedMemory(0xFE00 + 4*i);
		xPos = gbMemory.readMappedMemory(0xFE00 + 4*i + 1);
		tile = gbMemory.readMappedMemory(0xFE00 + 4*i + 2);
		attribute = gbMemory.readMappedMemory(0xFE00 + 4*i + 3);
		
		colorFlag = colorVal;
		colorPalette = attribute & 0x07;
		charBank = (attribute & 0x08) >> 3;
		dmgPalette = (attribute & 0x10) >> 4;
		flipx = (attribute & 0x20) == 0x20;
		flipy = (attribute & 0x40) == 0x40;
		bgPriority = (attribute & 0x80) == 0x80;
		largeBlock = (gbMemory.readMappedMemory(0xFF40) & 0x04) == 0x04;
		height = largeBlock ? 16 : 8;
		visible = true;
		index = i;
 	}
	
	int getPixel(int xCoord, int yCoord) {
		int tileDataOffset;
		if (flipy) {
			tileDataOffset = 0x8000 + (tile + ((yCoord > 7) ? 0 : 1)) * 0x10;
		}
		else tileDataOffset = 0x8000 + (tile + (yCoord/8)) * 0x10;
		
		if (yCoord > 7) yCoord -= 8;

		int tileLineLower = gbMemory.readMappedMemory(tileDataOffset + (flipy ? 7 - yCoord : yCoord)*2);
		int tileLineUpper = gbMemory.readMappedMemory(tileDataOffset + (flipy ? 7 - yCoord : yCoord)*2 + 1);
		
		//extracting the byte split char values is strange. I think this is the most efficient way in Java.
		int pixel = 0;
		if ((tileLineUpper & (0x01 << (flipx ? xCoord : 7-xCoord))) > 0) pixel += 2;
		if ((tileLineLower & (0x01 << (flipx ? xCoord : 7-xCoord))) > 0) pixel += 1;
		
		//Palette offset
		if (colorFlag) {
			//TODO implement colour paletting
		}
		
		else {
			pixel = (attribute & 0x10) == 0x10 ? pixel + 8 : pixel + 4;
		}
		
		return pixel;
	}
	
	int getx() {
		return xPos - 8;
	}
	
	int gety() {
		return yPos - 16;
	}
	
	int getIndex() {
		return index;
	}
	
	int getColorPalette() {
		return colorPalette;
	}
	
	int getDmgPalette() {
		return dmgPalette;
	}
	
	int charBank() {
		return charBank;
	}
	
	boolean getPriority() {
		return (attribute & 0x80) == 0x80;
	}
}
