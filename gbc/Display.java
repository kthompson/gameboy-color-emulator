package gbc;

import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex2f;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL13.GL_TEXTURE1;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.system.MemoryUtil.NULL;
import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.util.Scanner;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GLContext;

public class Display {
	
    private GLFWErrorCallback errorCallback;
 
    // handles
    private long window;
    private int colourShader;
    private int colourProgram;
    
    //shader arguments
    private int shaderPalette;
    private int shaderFrameBuffer;
    
    private int shaderPaletteLoc;
    private int shaderFrameBufferLoc;
    
    int WIDTH = 160;
    int HEIGHT = 144;
    int scale = 4;
 
    public Display() {
    	init();
    }
 
    void init() {
    	//Link to the lwjgl libraries
    	//XXX keep up to date
    	System.setProperty("org.lwjgl.librarypath", new File("src/lwjgl/native/").getAbsolutePath());
    	
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( glfwInit() != GL11.GL_TRUE )
            throw new IllegalStateException("Unable to initialize GLFW");
 
        // Configure our window. The gameboy display is 160x144, the initial size, but it can be scaled
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
 
        // Create the window
        window = glfwCreateWindow(WIDTH, HEIGHT, "Kemu", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");
 
        // Make the OpenGL context current and make openGL bindings available
        glfwMakeContextCurrent(window);
        glfwSwapInterval(1);
        
    	GLContext.createFromCurrent();
        
    	//Compile our colouriser shader
    	try {
    		File shaderSource = new File("src//gbc//palette.texture");
    		Scanner sourceReader = new Scanner(shaderSource);
			String textureShaderSource = sourceReader.useDelimiter("\\A").next();
			sourceReader.close();
	    	colourShader = glCreateShader(GL_FRAGMENT_SHADER);
	    	glShaderSource(colourShader, textureShaderSource);
		} 	
    	catch (FileNotFoundException e) {
			System.out.println("Palette.texture shader source read or compilation failure.");
			e.printStackTrace();
		}
    	
    	
    	glCompileShader(colourShader);
    	colourProgram = glCreateProgram();
    	glAttachShader(colourProgram, colourShader);
    	glLinkProgram(colourProgram);
    	
    	int maxLength = 10000;
    	ByteBuffer length = BufferUtils.createByteBuffer(4);
    	ByteBuffer log = BufferUtils.createByteBuffer(maxLength);
    	glGetShaderInfoLog(colourShader, maxLength, length, log);
    	byte[] bytes = new byte[log.remaining()];
    	log.get(bytes);
    	
    	String logString = new String( bytes, Charset.forName("UTF-8") );
    	System.out.println(logString);
    	
    	length = BufferUtils.createByteBuffer(4);
    	log = BufferUtils.createByteBuffer(maxLength);
    	glGetProgramInfoLog(colourProgram, maxLength, length, log);
    	bytes = new byte[log.remaining()];
    	log.get(bytes);
    	
    	logString = new String( bytes, Charset.forName("UTF-8") );
    	System.out.println(logString);
    	
    	shaderPalette = glGenTextures();
    	shaderFrameBuffer = glGenTextures();
    	
    	shaderPaletteLoc = glGetUniformLocation(colourProgram, "palette");
    	shaderFrameBufferLoc = glGetUniformLocation(colourProgram, "frameBuffer");
    	
    	//Set the viewport
        glViewport( 0, 0, WIDTH, HEIGHT );

        //Initialize Projection Matrix
        glMatrixMode( GL_PROJECTION );
        glLoadIdentity();
        glOrtho( 0.0, WIDTH, HEIGHT, 0.0, 1.0, -1.0 );

        //Initialize Modelview Matrix
        glMatrixMode( GL_MODELVIEW );
        glLoadIdentity();
        
        //Bind blank textures for frambuffer and palette
        glActiveTexture(GL_TEXTURE1);
        glBindTexture( GL_TEXTURE_2D, shaderPalette);
        GL20.glUniform1i(shaderPaletteLoc, 1);

	    ByteBuffer blankPalette = BufferUtils.createByteBuffer(64);
        blankPalette.position(0);
        
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, 64, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, blankPalette );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
        
		glActiveTexture(GL_TEXTURE0);
        glBindTexture( GL_TEXTURE_2D, shaderFrameBuffer );
        GL20.glUniform1i(shaderFrameBufferLoc, 0);
        
        ByteBuffer blankFrame = BufferUtils.createByteBuffer(WIDTH * HEIGHT);
        blankFrame.position(0);
        
        glTexImage2D( GL_TEXTURE_2D, 0, GL30.GL_R8I, WIDTH, HEIGHT, 0, GL30.GL_RED_INTEGER, GL_UNSIGNED_BYTE, blankFrame );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        
        // The window is fully set up, we can now make it visible
        glfwShowWindow(window);
    }
 
    void update() {
    	GLContext.createFromCurrent();
        // Poll for window events, including key inputs.
        glfwPollEvents();
        
        if ( glfwWindowShouldClose(window) == GL_TRUE ) { terminate();
        }
    }
    
    void drawFrame(ByteBuffer frameBuffer, ByteBuffer palette) {
    	GLContext.createFromCurrent();
    	
    	//Activate shader
    	glUseProgram(colourProgram);
	    
        //Find the window size
        IntBuffer windowWidth = BufferUtils.createIntBuffer(1);
        IntBuffer windowHeight = BufferUtils.createIntBuffer(1);
        glfwGetWindowSize(window, windowWidth, windowHeight);
		
    	//Set the viewport
        glViewport( 0, 0, windowWidth.get(0), windowHeight.get(0) );

        //Initialize Projection Matrix
        glMatrixMode( GL_PROJECTION );
        glLoadIdentity();
        glOrtho( 0.0, windowWidth.get(0), windowHeight.get(0), 0.0, 1.0, -1.0 );
        
        glActiveTexture(GL_TEXTURE1);
        glBindTexture( GL_TEXTURE_2D, shaderPalette);
        GL20.glUniform1i(shaderPaletteLoc, 1);

        palette.position(0);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 64, 1, GL_RGBA, GL_UNSIGNED_BYTE, palette);
		
		glActiveTexture(GL_TEXTURE0);		
        glBindTexture( GL_TEXTURE_2D, shaderFrameBuffer );
		GL20.glUniform1i(shaderFrameBufferLoc, 0 );
        
        frameBuffer.position(0);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 160, 144, GL30.GL_RED_INTEGER, GL_UNSIGNED_BYTE, frameBuffer);
		
        //Move to rendering point
        glTranslatef( 0.f, 0.f, 0.f );

        //Render textured quad
        glBegin( GL_QUADS );
            GL13.glMultiTexCoord2f(GL_TEXTURE1, 0.f, 0.f );
            GL13.glMultiTexCoord2f(GL_TEXTURE0, 0.f, 0.f );
            glVertex2f(0.f, 0.f );
            
            GL13.glMultiTexCoord2f(GL_TEXTURE1, 1.f, 0.f );
            GL13.glMultiTexCoord2f(GL_TEXTURE0, 1.f, 0.f );
            glVertex2f( windowWidth.get(0) + 0.5f, 0.f );
            

            GL13.glMultiTexCoord2f(GL_TEXTURE1, 1.f, 1.f );
            GL13.glMultiTexCoord2f(GL_TEXTURE0, 1.f, 1.f );
            glVertex2f( windowWidth.get(0) + 0.5f, windowHeight.get(0) + 0.5f);


            GL13.glMultiTexCoord2f(GL_TEXTURE1, 0.f, 1.f );
            GL13.glMultiTexCoord2f(GL_TEXTURE0, 0.f, 1.f );
            glVertex2f(0.f, windowHeight.get(0) + 0.5f);
        glEnd();
        
        //Update screen
        glfwSwapBuffers(window);
    }
    
    void terminate() {
    	errorCallback.release();
    	glfwDestroyWindow(window);
    	glfwTerminate();
    }
    
    long getWindowHandle() {
    	return window;
    }
    
    void attachKeyCallback(GLFWKeyCallback keyCallback) {
        // Key callback which is called when any key is pressed, released, or repeated
        glfwSetKeyCallback(window, keyCallback);
    }
    
    void closeWindow() {
    	glfwSetWindowShouldClose(window, GL_TRUE);
    }
}
