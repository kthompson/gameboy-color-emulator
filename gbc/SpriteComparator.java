package gbc;

import gbc.Sprite;
import java.util.Comparator;

public class SpriteComparator implements Comparator<Sprite> {
	boolean color = false;
	
	public int compare(Sprite s1, Sprite s2) {
		if (color || (s1.getx() == s2.getx())) {
			if (s1.getIndex() < s2.getIndex()) return -1;
			else return 1;
		}
		
		else {
			if (s1.getx() < s2.getx()) return -1;
			else return 1;
		}
	}
}
