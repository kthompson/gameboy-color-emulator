/*
 * The video class implements the gameboy's video logic. It should be run once per a scan line
 * to get the effect of per line processing. X background distortion, selective window display, etc.
 * Display update is far more processor intensive and could benefit from variable framerate
 */

package gbc;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;

public class Video {
	
	boolean color;
	Memory gbMemory;
	int wPos = 0;
	
	public Video(boolean colorFlag, Memory memoryRef) {
		color = colorFlag;
		gbMemory = memoryRef;
	}
	
	public void FrameRegUpdate() {
		wPos = 0;
	}
	
	public void DisplayRegUpdate() {
		if ((gbMemory.readMappedMemory(0xFF40) & 0x80) == 0x80) {
			//TODO add scanline mode updates
			int ly = gbMemory.readMappedMemory(0xFF44);
			
			//update stat register
			int stat = gbMemory.readMappedMemory(0xFF41);
			
			if (ly > 143) {
				stat = (stat & 0b11111100) | 0x01;
			}
			//XXX if not in vblank, assume hblank
			else stat = (stat & 0b11111100) | 0x00;
			
			int lyc = gbMemory.readMappedMemory(0xFF45);
			stat = (lyc == ly) ? (stat | 0x04) : (stat & 0b11111011);
			
			//XXX documentation implies writing to STAT resets bit 3. Mandates direct access here
			gbMemory.gbMem.cpuRam[0x41] = stat;
			//gbMemory.writeMappedMemory(0xFF41, stat);
		}
		
		else {
			gbMemory.writeMappedMemory(0xFF44, 0);
			int stat = gbMemory.readMappedMemory(0xFF41);
			stat = (stat & 0b11111100) | 0x00;
			gbMemory.gbMem.cpuRam[0x41] = stat;
		}
	}
	
	public void ScanLineUpdate(ByteBuffer framebuffer) {
		byte[] scanline = new byte[160];
		
		int ly = gbMemory.readMappedMemory(0xFF44);
		int lcdc = gbMemory.readMappedMemory(0xFF40);
		
		//Calculate background video
		if (color || (lcdc & 0x01) == 0x01) {
			int scx = gbMemory.readMappedMemory(0xFF43);
			int scy = gbMemory.readMappedMemory(0xFF42);

			//System.out.println("(" + wx + " , " + wy + ")");
			
			//TODO scrolling problems System.out.println("(" + scx + " , " + scy + ")");
			
			int backgroundMapOffset = ((lcdc & 0x08) == 0x08) ? 0x9C00 : 0x9800;
			int backgroundDataOffset = ((lcdc & 0x10) == 0x10) ? 0x8000: 0x8800;
			
			int bcps = gbMemory.readMappedMemory(0xFF68);
			int bcpd = gbMemory.readMappedMemory(0xFF69);
			
			int vbankTemp = gbMemory.readMappedMemory(0xFF4F);
			
			//TODO add colour support
			//Fill the scanline with background data
			for (int i = 0; i < 160; i++) {				
				int tile = backgroundMapOffset + ((scx + i)%256)/8 + (((scy + ly)%256)/8)*32;
				int tileData = gbMemory.readMappedMemory(tile);
				
				if (color)
				{
					gbMemory.writeMappedMemory(0xFF4F, 1);
					int colorData = gbMemory.readMappedMemory(tile);
					gbMemory.writeMappedMemory(0xFF4F, ((colorData & 0x08) == 0x08) ? 1 : 0);
					//offset by the color palette
					scanline[i] = (byte) (4 * (colorData & 0b111));
				}
				
				//TODO add color horizontal and vertical flipping
				
				if ((lcdc & 0x10) == 0) tileData = (tileData >= 0x80) ? tileData - 0x80 : tileData + 0x80;
				
				int tileLineLower = gbMemory.readMappedMemory(backgroundDataOffset + tileData * 0x10 + ((scy + ly)%8)*2);
				int tileLineUpper = gbMemory.readMappedMemory(backgroundDataOffset + tileData * 0x10 + ((scy + ly)%8)*2 + 1);
				
				//extracting the byte split char values is strange. I think this is the most efficient way in Java.
				if ((tileLineUpper & (0x01 << (7-((i + scx)%8)))) > 0) scanline[i] += 2;
				if ((tileLineLower & (0x01 << (7-((i + scx)%8)))) > 0) scanline[i] += 1;
			}
			
			//set to the start of the scanline and transfer into the framebuffer bytebuffer
			framebuffer.position(ly*160);
			framebuffer.put(scanline, 0, 160);
			
			gbMemory.writeMappedMemory(0xFF4F, vbankTemp);
		}
		
		//Overlay the window
		if ((lcdc & 0x20) == 0x20) {
			int wx = gbMemory.readMappedMemory(0xFF4B) - 7;
			int wy = gbMemory.readMappedMemory(0xFF4A);
			
			//XXX Link's Awakening hack
			if (wx < 0) wx = 0;
			
			if ((wx < 160) && (ly >= wy)) {
				int windowMapOffset = ((lcdc & 0x40) == 0x40) ? 0x9C00 : 0x9800;
				int windowDataOffset = ((lcdc & 0x10) > 0) ? 0x8000: 0x8800;
				
				int bcps = gbMemory.readMappedMemory(0xFF68);
				int bcpd = gbMemory.readMappedMemory(0xFF69);
				
				int vbankTemp = gbMemory.readMappedMemory(0xFF4F);
				
				for (int i = 0; i + wx < 160; i++) {
					int tile = windowMapOffset + i/8 + (wPos/8)*32;				
					int tileData = gbMemory.readMappedMemory(tile);

					scanline[i] = 0;
					if (color)
					{
						gbMemory.writeMappedMemory(0xFF4F, 1);
						int colorData = gbMemory.readMappedMemory(tile);
						gbMemory.writeMappedMemory(0xFF4F, ((colorData & 0x08) == 0x08) ? 1 : 0);
						//offset by the color palette
						scanline[i] = (byte) (4 * (colorData & 0b111));
					}
					
					//TODO add color horizontal and vertical flipping
					
					if ((lcdc & 0x10) == 0) tileData = (tileData >= 0x80) ? tileData - 0x80 : tileData + 0x80;

					int tileLineLower = gbMemory.readMappedMemory(windowDataOffset + tileData * 0x10 + (wPos%8)*2);
					int tileLineUpper = gbMemory.readMappedMemory(windowDataOffset + tileData * 0x10 + (wPos%8)*2 + 1);
					
					if ((tileLineUpper & (0x01 << (7-(i%8)))) > 0) scanline[i] += 2;
					if ((tileLineLower & (0x01 << (7-(i%8)))) > 0) scanline[i] += 1;
				}
				
				//Move to the next line in the window
				wPos++;
				
				//While you can't scroll the window you can set where the window is output to the screen. A small but important distinction
				framebuffer.position(ly*160 + wx);
				framebuffer.put(scanline, 0, 160 - wx);
				
				gbMemory.writeMappedMemory(0xFF4F, vbankTemp);
			}
		}
	}
	
	public void frameSprites(ByteBuffer framebuffer) {
		int LCDC = gbMemory.readMappedMemory(0xFF40);
		if ((LCDC & 0x02) == 0x02) {
			int ocps = gbMemory.readMappedMemory(0xFF6A);
			int ocpd = gbMemory.readMappedMemory(0xFF6B);
			
			boolean largeSprite = ((gbMemory.readMappedMemory(0xFF40) & 0x04) == 0x04);
			
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			
			for (int k = 0; k < 40; k++) {
				Sprite a = new Sprite(gbMemory, color, k);
				sprites.add(a);
			}
			
			//Now the sprites are all created we need to sort their priority
			SpriteComparator compare = new SpriteComparator();
			compare.color = color;
			Collections.sort(sprites, compare);
			
			int vbankTemp = gbMemory.readMappedMemory(0xFF4F);
			
			for (int k = 39; k > -1; k--) {
				int x = sprites.get(k).getx();
				int y = sprites.get(k).gety();
				int pixelOffset = 0;
				
				if (color)	pixelOffset = (byte) 4*sprites.get(k).colorPalette + 32;
				
				if (!largeSprite) {
					for (int i = 0; i < 8; i++) {
						for (int j = 0; j < 8; j++) {
							if (i + y > 0 && i + y < 144 && j + x > 0 && j + x < 160) {
								framebuffer.position(160*(i+y) + (j+x));
								int pixel = sprites.get(k).getPixel(j, i);
								if (pixel%4 != 0) { //is the pixel transparent?
									//Is the sprite in front of the window/background
									if ((sprites.get(k).getPriority() && (framebuffer.get(framebuffer.position())%4) == 0) || !sprites.get(k).getPriority())
									framebuffer.put((byte) (pixel + pixelOffset));
								}
							}
						}
					}
				}
				
				else {
					for (int i = 0; i < 16; i++) {
						for (int j = 0; j < 8; j++) {
							if (i + y > 0 && i + y < 144 && j + x > 0 && j + x < 160) {
								framebuffer.position(160*(i+y) + (j+x));
								int pixel = sprites.get(k).getPixel(j, i);
								if (pixel%4 != 0) { //is the pixel transparent?
									//Is the sprite in front of the window/background
									if ((sprites.get(k).getPriority() && (framebuffer.get(framebuffer.position())%4) == 0) || !sprites.get(k).getPriority())
									framebuffer.put((byte) (pixel + pixelOffset));
								}
							}
						}
					}
				}
				gbMemory.writeMappedMemory(0xFF4F, vbankTemp);
			}
		}
	}
	
	public void updatePalette(ByteBuffer palette) {
		palette.position(0);
		if (color) {
			//TODO implement colour
			return;
		}
		
		else {
			byte[] grayscales = {(byte) 255, (byte) 170, 85, 0};
			
			int bgp = gbMemory.readMappedMemory(0xFF47);
			int obp0 = gbMemory.readMappedMemory(0xFF48);
			int obp1 = gbMemory.readMappedMemory(0xFF49);
			
			for (int i = 0; i < 4; i++) {
				palette.put(grayscales[(bgp & 0x03)]);
				palette.put(grayscales[(bgp & 0x03)]);
				palette.put(grayscales[(bgp & 0x03)]);
				palette.put((byte) 0.0);
				bgp = bgp >> 2;
			}
			
			for (int i = 0; i < 4; i++) {
				palette.put(grayscales[(obp0 & 0x03)]);
				palette.put(grayscales[(obp0 & 0x03)]);
				palette.put(grayscales[(obp0 & 0x03)]);
				palette.put((byte) 0.0);
				obp0 = obp0 >> 2;
			}
			
			for (int i = 0; i < 4; i++) {
				palette.put(grayscales[(obp1 & 0x03)]);
				palette.put(grayscales[(obp1 & 0x03)]);
				palette.put(grayscales[(obp1 & 0x03)]);
				palette.put((byte) 0.0);
				obp1 = obp1 >> 2;
			}
		}
	}
	
	public void defaultPalette(ByteBuffer palette) {
		palette.position(0);
		if (color) {
			byte[] grayscales = {0, 85, (byte) 170,  (byte) 255};
			for (int i = 0; i < 16; i++) {
				palette.put((byte) ((i < 4) ? grayscales[i] : 0));
				palette.put((byte) ((i < 4) ? grayscales[i] : 0.0));
				palette.put((byte) ((i < 4) ? grayscales[i] : 0.0));
				palette.put((byte) 0.0);
			}
		}
		
		else {
			byte[] grayscales = {(byte) 255, (byte) 170, 85, 0};
			for (int i = 0; i < 16; i++) {
				palette.put((byte) ((i < 4) ? grayscales[i] : 0));
				palette.put((byte) ((i < 4) ? grayscales[i] : 0.0));
				palette.put((byte) ((i < 4) ? grayscales[i] : 0.0));
				palette.put((byte) 0.0);
			}
		}
	}
}