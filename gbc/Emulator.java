package gbc;

import java.io.*;

import gbc.MemoryInterface.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
//XXX implement echos 
/*The addresses E000-FE00 appear to access the internal
RAM the same as C000-DE00. (i.e. If you write a byte to
address E000 it will appear at C000 and E000.
Similarly, writing a byte to C000 will appear at C000
and E000.)*/

public class Emulator {
	final static boolean debugFlag = false;
	
	boolean colorFlag;
	
	Memory gbMemory;
	Display gbDisplay;
	Video gbVideo;
	Input gbInput;
	
	static File romFile;
	static File ramFile;
	
	int buttonState;		//Used for button port interrupt.
	//TODO remove all requeststate if ultimately not needed
	//int requestState;		//Supports the undocumented feature that an interrupt can occur on a falling edge
	int oldstat;
	int oldly;
	
	//clock rates and timing
	final int clockRate = 4194304;
	final double instructionPeriod = 4/clockRate;
	
	//peripheral periods in instruction cycles
	final int refreshPeriod = 17556;
	final int scanlinePeriod = 114;
	final int rtcPeriod = refreshPeriod * 60;
	
	//Video buffer objects
	ByteBuffer framebuffer;
	ByteBuffer palette;
	
	public static void main(String[] args) throws IOException {
		/*if (args.length == 0) {
			System.out.println("Required Arguments - rom_file_path");
			return;
		}*/
		
		//TODO investigate pokemon card game crash (japanese)
		romFile = new File("Pokemon - Gold Version (UE) (C).gbc");
		//File romFile = new File(args[0]);
		
		ramFile = new File(romFile.getAbsoluteFile() + ".sav");
		
		boolean colorArg = false;
		
		Emulator gameboy = new Emulator(colorArg, romFile, ramFile);
		gameboy.Emulate();
	}
	
	public Emulator(boolean color, File romFile, File ramFile) {
		colorFlag = color;
		
		framebuffer = BufferUtils.createByteBuffer(160*144);
		palette = BufferUtils.createByteBuffer(2*8*4*4);	//4 colors in 8 palettes for each of background and objects. Each palette has 4 colours.
		
		//Initialise memory object
		gbMemory = new Memory(colorFlag, romFile, ramFile, palette);
		
		//set to the beginning of the execution
		gbMemory.setRegPair(regPair.PC, 0x100);
		
		//Initialise video
		gbDisplay = new Display();
		
		//Initialise GLFW input
		gbInput = new Input(gbMemory);
		gbInput.attachToWindow(gbDisplay.getWindowHandle());
		gbDisplay.attachKeyCallback(gbInput.keyCallback);
		gbMemory.attachInput(gbInput);
		
	    
	    gbVideo = new Video(colorFlag, gbMemory);
	    if (!colorFlag) gbVideo.defaultPalette(palette);
	}
		
	void Emulate() {
		BufferedWriter debugOut = null;
		if (debugFlag) {
			try {
				debugOut = new BufferedWriter(new FileWriter("debug.log", false));
			} catch (IOException e) {
				System.out.println("Can't open debug file");
				e.printStackTrace();
			}
		}
		
		int instructionCount = 0, scanlineCount = scanlinePeriod, refreshCount = refreshPeriod, timerCount = 0, dividerCount = 0, rtcCount = 0;
		int breakpoint = -1;
		boolean doubleSkip = false;
		double periodStart = System.nanoTime();
		double saveStart = System.currentTimeMillis();
		
		buttonState = gbMemory.readMappedMemory(0xFF00);
		//requestState = 0;
		//Initial Memory setup
		gbVideo.DisplayRegUpdate();
		oldstat = gbMemory.readMappedMemory(0xFF41);
		oldly = gbMemory.readMappedMemory(0xFF44);
		
		while (instructionCount >= 0) {
			if (gbMemory.GetHalt() == false) {
				//debug break and step
				if (gbInput.playFlag == true && gbMemory.getRegPair(regPair.PC) == breakpoint) {
					System.out.println("Stopped at breakpoint at " + Integer.toHexString(gbMemory.getRegPair(regPair.PC)));
					gbInput.playFlag = false;
				}
				
				if (gbInput.playFlag == false && instructionCount == 0) {
					while (gbInput.stepFlag == false) {
						gbDisplay.update();
					}
					
					System.out.println("PC - " + Integer.toHexString(gbMemory.getRegPair(regPair.PC)) +
							"\tSP - " + Integer.toHexString(gbMemory.getRegPair(regPair.SP)));
					System.out.println("PC contents - " + Integer.toHexString(gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.PC))) + 
							"\tSP contents - " + Integer.toHexString(gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.SP))));
					System.out.println("A - " + Integer.toHexString(gbMemory.getReg(reg.A)) + 
							"\tF - " + Integer.toHexString(gbMemory.getReg(reg.F)));
					System.out.println("B - " + Integer.toHexString(gbMemory.getReg(reg.B)) + 
							"\tC - " + Integer.toHexString(gbMemory.getReg(reg.C)));
					System.out.println("D - " + Integer.toHexString(gbMemory.getReg(reg.D)) + 
							"\tE - " + Integer.toHexString(gbMemory.getReg(reg.E)));
					System.out.println("H - " + Integer.toHexString(gbMemory.getReg(reg.H)) + 
							"\tL - " + Integer.toHexString(gbMemory.getReg(reg.L)));
					System.out.println("mbc[0] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[0]) +
							"\tmbc[1] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[1]));
					System.out.println("mbc[2] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[2]) +
							"\tmbc[3] - " + Integer.toHexString(gbMemory.gbMem.mbcRegs[3]) + "\n");

					System.out.println("mbc retrieved - " + Integer.toHexString(gbMemory.gbMem.mbcRomBank()));
					for (int i = 0x0F; i > 0; i--) {
						System.out.println(Integer.toHexString(0xFDF2 + i) + " : " + Integer.toHexString(gbMemory.readMappedMemory(0xFDF2 + i)));
					}
					
					//Palette printing routine
					/*palette.position(0);
					for (int i = 0; i < 32; i++) {
						System.out.println("(" + palette.get() + ", " + palette.get() + ", " + palette.get() + ", " + palette.get() + ")");
					}*/
					
					gbInput.stepFlag = false;
				}
				
				periodStart = System.nanoTime();
				
				if (instructionCount == 0) {
					if (debugFlag) {
						try {
							debugOut.write(Integer.toHexString(gbMemory.getRegPair(regPair.PC)) + "\n");
							debugOut.write(Integer.toHexString(gbMemory.getRegPair(regPair.AF)) + "\n");
							debugOut.write(Integer.toHexString(gbMemory.getRegPair(regPair.BC)) + "\n");
							debugOut.write(Integer.toHexString(gbMemory.getRegPair(regPair.DE)) + "\n");
							debugOut.write(Integer.toHexString(gbMemory.getRegPair(regPair.HL)) + "\n");
							debugOut.write(Integer.toHexString(gbMemory.getRegPair(regPair.SP)) + "\n");
						} catch (IOException e) {
							System.out.print("Trouble writing to log");
							e.printStackTrace();
						}
					}
					
					//System.out.println(Integer.toHexString(gbMemory.getRegPair(regPair.PC)));
					instructionCount = CPU.executeNextOpCode(gbMemory);
				}
				instructionCount--;
			}
			
			ServiceInterrupt();
			
			boolean doubleSpeed = ((gbMemory.readMappedMemory(0xFF4D) & 0x80) == 0x80);
			
			//When in double speed mode skip peripheral step in odd numbered cycles
			if (!(doubleSpeed && doubleSkip)) {
				//process timer updates
				if (timerCount == 0) {
					timerCount = TimerUpdate(timerCount);
				}
				timerCount--;
				
				if (dividerCount == 0) {
					dividerCount = DividerUpdate(dividerCount);
				}
				dividerCount--;
				
				if (rtcCount == 0) {
					gbMemory.clock.advanceSecond();
					//System.out.println(gbMemory.clock.getDays() + " : " + gbMemory.clock.getHours() + " : " + gbMemory.clock.getMinutes() + " : " + gbMemory.clock.getSeconds());
					rtcCount = rtcPeriod;
				}
				rtcCount--;
				
				if ((gbMemory.readMappedMemory(0xFF40) & 0x80) == 0x80 || colorFlag) {
					//LCDC interrupts can happen whenever
					InterruptRequest(InterruptFlag.LCDC);
					
					//A new scanline occurs every 114 normal speed instruction cycles
					if (scanlineCount <= 0) {
						int scanline = gbMemory.readMappedMemory(0xFF44) + 1;
						
						if (scanline == 154) scanline = 0;
	
						gbMemory.writeMappedMemory(0xFF44, scanline);	//write back new scanLine value
						
						gbVideo.DisplayRegUpdate();
						
						if (scanline < 144) gbVideo.ScanLineUpdate(framebuffer);
						
						InterruptRequest(InterruptFlag.blank);			//Check if Vblank interrupt is now applicable
						
						scanlineCount = scanlinePeriod;
					}
					scanlineCount--;
					
					//A new frame occurs every 17556 normal speed instruction cycles
					if (refreshCount <= 0) {						
						//Add sprites to the framebuffer
						//gbVideo.updatePalette(palette);
						gbVideo.defaultPalette(palette);
						gbVideo.frameSprites(framebuffer);
						gbVideo.FrameRegUpdate();
						
						gbDisplay.drawFrame(framebuffer, palette);
						gbDisplay.update();
						
						//input related
						InterruptRequest(InterruptFlag.BTN);
						buttonState = gbMemory.readMappedMemory(0xFF00);
						
						refreshCount = refreshPeriod;
						
						if (gbInput.frameFlag) {
							gbInput.playFlag = false;
							gbInput.frameFlag = false;
							
							palette.position(0);
							for (int i = 0; i < palette.limit()/4; i++) {
								System.out.println(palette.get() + " " + palette.get() + " " + palette.get() + " " + palette.get());
							}
							
							framebuffer.position(0);
							for (int i = 0; i < 32; i ++) {
								for (int j = 0; j < 32; j++) {
									System.out.print(" " + gbMemory.readMappedMemory(0x9800 + i) + " ");
								}
								System.out.println();
							}
							
							gbDisplay.drawFrame(framebuffer, palette);
							gbDisplay.update();
						}
						
						//save state
						if (gbInput.saveState) {
							try {
								FileOutputStream saveFile = new FileOutputStream(romFile.getAbsolutePath() + romFile.getName() + "state.ser");
								ObjectOutputStream out = new ObjectOutputStream(saveFile);
								out.writeObject(gbMemory);
								out.close();
								saveFile.close();
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
							gbInput.saveState = false;
						}
						
						if (gbInput.loadState) {
							Input tempInput = (Input) gbMemory.buttons;
							try {
								System.out.print("Loading state");
								FileInputStream loadFile = new FileInputStream(romFile.getAbsolutePath() + romFile.getName() + "state.ser");
								ObjectInputStream in = new ObjectInputStream(loadFile);
								gbMemory = (Memory) in.readObject();
								in.close();
								loadFile.close();
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							}
							gbMemory.buttons = tempInput;
							gbInput.loadState = false;
						}
					}
					refreshCount--;
				}
				
				else {
					gbVideo.DisplayRegUpdate();
					scanlineCount = 0;
					refreshCount = 0;
				}
				
		        //any leftover loop time we spend giving to the input and video update routines. TODO add double time
				while ((System.nanoTime() - periodStart) * 1E-9 < instructionPeriod/((doubleSpeed) ? 2 : 1)) {
					gbDisplay.update();
				}
				
				//Save the file each second
				if (System.currentTimeMillis() - saveStart > 1000) {
					gbMemory.gbMem.saveRam(ramFile);
					saveStart = System.currentTimeMillis();
				}
			}
			doubleSkip = !doubleSkip;
		}
		
		//TODO remove
		System.out.println("Failure at PC - " + gbMemory.getRegPair(regPair.PC));
		System.out.println("Instruction was " + gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.PC) - 1));
		System.out.println("Previous was " + gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.PC) - 2));
		System.out.println("1st argument " + gbMemory.readMappedMemory(gbMemory.getRegPair(regPair.PC)));
	}
	
		
	//TODO finish interrupt checks
	void InterruptRequest(InterruptFlag interrupt) {
		switch (interrupt) {
		case BTN:	//check if a button input has gone low
			if ((buttonState & 0x0F) > (gbMemory.readMappedMemory(0xFF00) & 0x0F)) {
				gbMemory.setIR(interrupt, true);
			}
			break;
			
		case LCDC:	//Do 4 STAT determined interrupt checks
			//XXX are these transition interrupts or as implied will they interrupt again after reti
			int stat = gbMemory.readMappedMemory(0xFF41);
			if ((stat & 0x0B) == 0x08 || (stat & 0x13) == 0x11 || (stat & 0x23) == 0x22 || ((stat & 0x44) == 0x44 && ((oldstat & 0x04) == 0))) {
				gbMemory.setIR(interrupt, true);
			}
			oldstat = stat;
			break;
		case Serial:
			//TODO implement serial interrupt
			break;
		case TMR: //Interrupt request handled in timer module
			break;
		case blank:
			int scanLine = gbMemory.readMappedMemory(0xFF44);
			if (scanLine > 143 && oldly <= 143) {
				gbMemory.setIR(interrupt, true);
			}
			oldly = scanLine;
			break;
		}
	}
	
	void ServiceInterrupt() {
		//XXX Sources are unclear when request flag should be cleared but it makes the most sense here
		//TODO confirm state change hypothesis
		if (gbMemory.getMIE() || gbMemory.GetHalt()) {
			if (gbMemory.getIE(InterruptFlag.blank) && gbMemory.getIR(InterruptFlag.blank)) {//(requestState & 0x01) != (gbMemory.readMappedMemory(0xFF0F) & 0x01)) {
				if (gbMemory.getMIE()) gbMemory.setIR(InterruptFlag.blank, false);
				InterruptJump(0x40);
			}
			
			else if (gbMemory.getIE(InterruptFlag.LCDC) && gbMemory.getIR(InterruptFlag.LCDC)) {//(requestState & 0x01) != (gbMemory.readMappedMemory(0xFF0F) & 0x01)) {
				if (gbMemory.getMIE()) gbMemory.setIR(InterruptFlag.LCDC, false);
				InterruptJump(0x48);
			}
			
			else if (gbMemory.getIE(InterruptFlag.TMR) && gbMemory.getIR(InterruptFlag.TMR)) {
				if (gbMemory.getMIE())gbMemory.setIR(InterruptFlag.TMR, false);
				InterruptJump(0x50);
			}
			
			else if (gbMemory.getIE(InterruptFlag.Serial) && gbMemory.getIR(InterruptFlag.Serial)) {
				if (gbMemory.getMIE())gbMemory.setIR(InterruptFlag.Serial, false);
				InterruptJump(0x58);
			}
			
			else if (gbMemory.getIE(InterruptFlag.BTN) && gbMemory.getIR(InterruptFlag.BTN)) {
				if (gbMemory.getMIE())gbMemory.setIR(InterruptFlag.BTN, false);
				InterruptJump(0x60);
			}
			//requestState = gbMemory.readMappedMemory(0xFF0F);
		}
	}
	
	void InterruptJump(int addr) {
		gbMemory.SetHalt(false);
		if (gbMemory.getMIE()) {
			gbMemory.setMIE(false);
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), (gbMemory.getRegPair(regPair.PC) >>> 8 ) & 0xFF);
			gbMemory.setRegPair(regPair.SP, gbMemory.getRegPair(regPair.SP) - 1);
			gbMemory.writeMappedMemory(gbMemory.getRegPair(regPair.SP), gbMemory.getRegPair(regPair.PC) & 0xFF);
			
			gbMemory.setRegPair(regPair.PC, addr);
		}
	}
	
	int TimerUpdate(int timerCount) {
		int tac = gbMemory.readMappedMemory(0xFF07);

		if ((tac & 0x04) == 0x04) {
			switch (tac & 0x03) {
			case 0: timerCount = 256;
					break;
			case 1: timerCount = 4;
					break;
			case 2: timerCount = 16;
					break;
			case 3: timerCount = 64;
					break;
			}
		
			int tima = gbMemory.readMappedMemory(0xFF05);
			int tma = gbMemory.readMappedMemory(0xFF06);
			
			if (++tima == 0x100) {
				tima = tma;
				gbMemory.setIR(InterruptFlag.TMR, true);
			}
			
			gbMemory.writeMappedMemory(0xFF06, tma);
			gbMemory.writeMappedMemory(0xFF05, tima);
		}
		
		else {
			timerCount = 1;
			int tima = 0;
			gbMemory.writeMappedMemory(0xFF05, tima);
		}		
		return timerCount;
	}
	
	int DividerUpdate(int dividerCount) {
		dividerCount = 128;
		int div = gbMemory.readMappedMemory(0xFF04);
		if (div++ == 0x100) div = 0;
		//So disgusting, so taboo. Required as all normal writes to DIV reset the register.
		gbMemory.gbMem.cpuRam[0x04] = div;
		return dividerCount;
	}
}