import re

masterLog = open("tracer\\trace.log", "r")
masterLines = masterLog.readlines()
#print lines
masterInstructions = len(masterLines)
print "Master instructions captured - " + str(masterInstructions)
masterLog.close()

slaveLog = open("debug.log")
slaveLines = slaveLog.readlines()
slaveInstructions = len(slaveLines)
print "Slave instructions captured - " + str(slaveInstructions)
slaveLog.close()

instructionMax = masterInstructions
if instructionMax > slaveInstructions:
    instructionMax = slaveInstructions

for i in range(instructionMax):
    searchObj = re.search('^\[([0-9A-Fa-f]{6})\]\s.+$', masterLines[i])
    addr = searchObj.group(1)
    print str(int(addr, 16)) + " - " + str(int(slaveLines[i].rstrip(), 16))
